//
//  NotificationsShared.swift
//  geowave
//
//  Created by Doug Chisholm on 31/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import UIKit

struct GeowaveNotification {
	
	let _userInfo:[AnyHashable: Any]
	
	init(userInfo: [AnyHashable: Any]) {
		_userInfo = userInfo
	}
	
	var isGeowave: Bool {
		
		guard _userInfo["gwCampaignID"] != nil else {
			return false
		}
		
		return true
	}
	
	var CampaignID: Int {
		
		if let campaignID = _userInfo["gwCampaignID"] {
			return campaignID as! Int
		}
		return 0
	}
	
	var Url: String {
		
		if let url = _userInfo["gwURL"] {
			return url as! String
		}
		return ""
	}
	
	func applyUrlChanges(url: String) -> String {
		
		var finalUrl = url
		
		finalUrl = finalUrl.replacingOccurrences(of: "@AdvertisingID", with: UserSettings.current.AdvertisingID)
		finalUrl = finalUrl.replacingOccurrences(of: "@AppID", with: String(UserSettings.current.AppID))
		finalUrl = finalUrl.replacingOccurrences(of: "@CampaignID", with: String(self.CampaignID))
		finalUrl = finalUrl.replacingOccurrences(of: "@HandsetUDID", with: UserSettings.current.HandsetUDID)
		
		if let lastOpen = UserActions.current.AppOpens.last {
			
			if let bestLocation = Utils.getBestLocation(locations: lastOpen.Locations) {
				finalUrl = finalUrl.replacingOccurrences(of: "@Latitude", with: String(bestLocation.coordinate.latitude))
				finalUrl = finalUrl.replacingOccurrences(of: "@Longitude", with: String(bestLocation.coordinate.longitude))
			}
		}
		
		return finalUrl
	}
	
}

class NotificationsShared {
	
	class func process(userInfo: [AnyHashable: Any]) {
		let notification = GeowaveNotification.init(userInfo: userInfo)
		
		if notification.isGeowave {
			if notification.CampaignID != 0 {
				//GeowaveService.TrackPushFeedback(campaignID: notification.CampaignID)
			}
			
			if notification.Url.count > 0 {
				
				Geowave.current.urlView.dismiss(animated: false, completion: nil)
				Geowave.current.urlView.showUrl(url: "\(notification.Url)", campaignID: notification.CampaignID)
				
				Utils.delay(1.5, closure: {
					UIApplication.shared.windows.last?.rootViewController?.present(Geowave.current.urlView, animated: true, completion: { })
				})
			}
		}
	}
}
