//
//  UserLocation
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

class Location : NSObject, CLLocationManagerDelegate {
	
	// MARK: declarations
	
	var LocationManager: CLLocationManager!
	var isUsingFullLocation : Bool = false
	
	static let current : Location = {
		let current = Location()
		
		current.LocationManager = CLLocationManager()
		current.LocationManager.delegate = current
		current.LocationManager.desiredAccuracy = kCLLocationAccuracyBest
		
		return current
		
	}()
	
	// MARK: Location Start/Stop
	
	class func start() {
		
		Log.add(detailedLog: "Location Updates Started")
		
		Location.current.LocationManager.startUpdatingLocation()
		
		Location.current.LocationManager.startMonitoringVisits()
		Location.current.LocationManager.startMonitoringSignificantLocationChanges()
		
		Location.current.isUsingFullLocation = true
		
		Utils.delay(30.0) {
			Location.stop()
		}
	}
	
	class func stop() {
		
		Log.add(detailedLog: "Location Updates Stopped")
		current.LocationManager.stopUpdatingLocation()
		
		current.isUsingFullLocation = false
		
	}
	
	// MARK: Request Authorization
	
	class func request(with type: LocationType) {
		
		switch(type) {
			
		case .Always:
			Log.add(detailedLog: "Location Always Authorization Requested")
			current.LocationManager.requestAlwaysAuthorization()
			break
		case .WhenInUse:
			Log.add(detailedLog: "Location When In Use Authorization Requested")
			current.LocationManager.requestWhenInUseAuthorization()
			break
		}
		
	}
	
	func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
		
		//check settings
		var hasLocationEnabled = false
		
		let elements =
			[CLAuthorizationStatus.authorizedAlways.rawValue,
			 CLAuthorizationStatus.authorizedWhenInUse.rawValue]
		
		if elements.contains(status.rawValue) {
			hasLocationEnabled = true
			
			Location.start()
			UserLocation.setupBeacons()
		}
		
		//update settings
		UserSettings.set(HasLocationEnabled: hasLocationEnabled);
		UserSettings.save()
		
	}
	
	// MARK: CLLocation Manager Delegate methods
	
	func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
		
		Log.add(detailedLog: "didVisit: \(visit)")
		UserActions.add(visit: visit)
		
	}
	
	func locationManagerDidPauseLocationUpdates(_ manager: CLLocationManager) {
		
		Log.add(detailedLog: "DidPauseLocationUpdates")
		
	}
	
	func locationManagerDidResumeLocationUpdates(_ manager: CLLocationManager) {
		
		Log.add(detailedLog: "DidResumeLocationUpdates")
		
	}
	
	func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
		
		Log.add(detailedLog: "DidResumeLocationUpdates")
		
	}
	
	func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
		
		Location.current.LocationManager.stopRangingBeacons(in: region as! CLBeaconRegion)
		
		print("Exit Region \(region.identifier)")
		
		Log.add(detailedLog: "didExitRegion: \(region)")
		
	}
	
	func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
		
		print("Enter Region \(region.identifier)")
		
		Location.current.LocationManager.startRangingBeacons(in: region as! CLBeaconRegion)
		
		Log.add(detailedLog: "didEnterRegion: \(region)")
	}
	
	func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
		
		Log.add(detailedLog: "didStartMonitoringFor: \(region)")
		
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
		
		Log.add(detailedLog: "didUpdateHeading: \(newHeading)")
		
	}
	
	func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		UserActions.add(locations: locations)
	}
	
	func locationManager(_ manager: CLLocationManager, didFinishDeferredUpdatesWithError error: Error?) {
		
		Log.add(detailedLog: "didFinishDeferredUpdatesWithError: \(String(describing: error))")
		
	}
	
	func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
		
		Log.add(detailedLog: "didDetermineState: State->\(state) Region->\(region)")
	}
	
	func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
		
		print("range beacons in \(region.identifier) (\(beacons.count))" )

		Log.add(detailedLog: "didRangeBeacons: Beacons->\(beacons) Region->\(region)")
	}
	
	func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
		
		Log.add(detailedLog: "monitoringDidFailFor: region->\(String(describing: region)) Error->\(error)")
	}
	
	func locationManager(_ manager: CLLocationManager, rangingBeaconsDidFailFor region: CLBeaconRegion, withError error: Error) {
		
		Log.add(detailedLog: "rangingBeaconsDidFailFor: region->\(region) Error->\(error)")
	}
}
