//
//  GeowaveService.swift
//  geowave
//
//  Created by Doug Chisholm on 25/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

class GeowaveService {
	
	static let current: GeowaveService = {
		
		let current = GeowaveService()
		return current
		
	}()
	
	var requests = [URLRequest]()
	var lastRetry = Date()
	var isRetrying = false
	
	// MARK: API CALLS
	
	class func RegisterDevice() {
		
		Log.add(detailedLog: "API Call: Register Device")
		
		let keys: [String] = ["UUID", "IsLimited", "Platform", "AppID", "DeviceModel", "SystemVersion", "AppVersion", "SDKVersion", "CreateDate"]
		
		let values: [String] = [
			
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(UserSettings.current.AppID),
			UserSettings.current.DeviceModel,
			UserSettings.current.SystemVersion,
			UserSettings.current.AppVersion,
			UserSettings.current.SDKVersion,
			//UserSettings.current.DeviceName,
			Utils.getDateInUTC(date: Date())
		]
		
		if UserDefaults.standard.bool(forKey: "allowTracking") {
			GeowaveService.callBit(api: API.RegisterDevice, keys: keys, values: values)
		}
	}
	
	class func TrackLocation(location: CLLocation) {
		
		Log.add(detailedLog: "API Call: Track Location")
		
		var keys: [String]        = ["UUID", "IsLimited", "Platform", "AppID", "CreateDate"]
		var values: [String]      = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(UserSettings.current.AppID),
			Utils.getDateInUTC(date: Date())
		]
		
		keys.append("Latitude")
		values.append(String(location.coordinate.latitude))
		
		keys.append("Longitude")
		values.append(String(location.coordinate.longitude))
		
		keys.append("Altitude")
		values.append(String(location.altitude))
		
		keys.append("Speed")
		values.append(String(location.speed))
		
		keys.append("Accuracy")
		values.append(String(Int(location.horizontalAccuracy)))
		
		if UserDefaults.standard.bool(forKey: "allowTracking") {
			GeowaveService.callBit(api: API.Location, keys: keys, values: values)
		}
	}
	
	class func TrackFrequentLocation(visit: CLVisit) {
		
		Log.add(detailedLog: "API Call: Track Visit")
		
		var keys: [String]        = ["UUID", "IsLimited", "Platform", "AppID", "CreateDate"]
		var values: [String]      = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(UserSettings.current.AppID),
			Utils.getDateInUTC(date: Date())
		]
		
		keys.append("Latitude")
		values.append(String(visit.coordinate.latitude))
		
		keys.append("Longitude")
		values.append(String(visit.coordinate.longitude))
		
		keys.append("Accuracy")
		values.append(String(Int(visit.horizontalAccuracy)))
		
		if visit.arrivalDate != Date.distantPast {
			keys.append("ArrivalDate")
			values.append(Utils.getDateInUTC(date: visit.arrivalDate))
		}
		
		if visit.departureDate != Date.distantFuture {
			keys.append("DepartureDate")
			values.append(String(Utils.getDateInUTC(date: visit.departureDate)))
		}
		
		if UserDefaults.standard.bool(forKey: "allowTracking") {
			GeowaveService.callBit(api: API.FrequentLocation, keys: keys, values: values)
		}
		
	}
	
	class func TrackAppOpen(appOpen: AppOpen) {
		
		Log.add(detailedLog: "API Call: Track App Open")
		
		var keys: [String] = ["UUID", "IsLimited", "Platform", "AppID", "CreateDate"]
		var values: [String] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(UserSettings.current.AppID),
			Utils.getDateInUTC(date: Date())
		]
		
		keys.append("SessionTime")
		values.append(String(Int(appOpen.sessionTime)))
		
		if let bestLocation = Utils.getBestLocation(locations: appOpen.Locations) {
			keys.append("Latitude")
			values.append(String(bestLocation.coordinate.latitude))
			keys.append("Longitude")
			values.append(String(bestLocation.coordinate.longitude))
			keys.append("Accuracy")
			values.append(String(Int(bestLocation.horizontalAccuracy)))
			keys.append("Altitude")
			values.append(String(bestLocation.altitude))
			keys.append("Speed")
			values.append(String(bestLocation.speed))
		}
		
		if UserDefaults.standard.bool(forKey: "allowTracking") {
			GeowaveService.callBit(api: API.AppOpen, keys: keys, values: values)
		}
	}
	
	class func TrackPushSettings() {
		
		Log.add(detailedLog: "API Call: Track Push Settings")
		
		let keys: [String] = ["UUID", "IsLimited", "Platform", "AppID", "HasPushEnabled", "PushToken", "CreateDate"]
		let values: [String] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(UserSettings.current.AppID),
			String(UserSettings.current.HasPushEnabled),
			UserSettings.current.DeviceToken,
			Utils.getDateInUTC(date: Date())
		]
		if UserDefaults.standard.bool(forKey: "allowTracking") {
			GeowaveService.callBit(api: API.PushSettings, keys: keys, values: values)
		}
	}
	
	class func TrackPersonalDetails() {
		
		Log.add(detailedLog: "API Call: Track Personal Details")
		
		let keys: [String] = ["UUID", "IsLimited", "Platform", "AppID", "Male", "DoB", "CreateDate"]
		let values: [String] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(UserSettings.current.AppID),
			String(UserDefaults.standard.bool(forKey: "pd_gender")),
			Utils.getDateInUTC(date: UserDefaults.standard.value(forKey: "pd_dob") as? Date ?? Date()),
			Utils.getDateInUTC(date: Date())
		]
		GeowaveService.callBit(api: API.PersonalDetails, keys: keys, values: values)
	}
	
	class func TrackOnboardDetails(optedInAt: Int) {
		
		Log.add(detailedLog: "API Call: Track Onboard Details")
		
		let keys: [String] = ["Uuid", "DeviceModel", "DeviceBrand", "NetworkProvider", "ConnectionType", "Appid", "Platform", "OptedInAt", "TimeStamp"]
		let values: [String] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			UserSettings.current.DeviceModel,
			"Apple",
			"",
			(Reachability()?.currentReachabilityStatus.description)!,
			String(UserSettings.current.AppID),
			"ios",
			String(optedInAt),
			Utils.getDateInUTC(date: Date())
		]
		GeowaveService.callBit(api: API.OnboardDetails, keys: keys, values: values)
	}
	
	//  class func TrackPushFeedback(campaignID: Int) {
	//
	//    Log.add(detailedLog: "API Call: Track Push Feedback")
	//
	//    var keys: [String] = ["HandsetUDID", "AppID", "IsOpen", "IsReceived", "IsReceivedForeground", "CampaignID", "CreateDate"]
	//    var values: [String] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      String(true),
	//      String(false),
	//      String(false),
	//      String(campaignID),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    if let lastOpen = UserActions.current.AppOpens.last {
	//
	//      if let bestLocation = Utils.getBestLocation(locations: lastOpen.Locations) {
	//
	//        keys.append("Latitude")
	//        values.append(String(bestLocation.coordinate.latitude))
	//        keys.append("Longitude")
	//        values.append(String(bestLocation.coordinate.longitude))
	//        keys.append("Accuracy")
	//        values.append(String(Int(bestLocation.horizontalAccuracy)))
	//      }
	//    }
	//
	//    GeowaveService.call(api: API.PushFeedback, keys: keys, values: values)
	//  }
	
	class func TrackLoyaltyFeedback(id: Int, isScan: Bool, isRedeem: Bool, stamps: Int) {
		
		Log.add(detailedLog: "API Call: Track Loyalty Feedback")
		
		let keys: [String] = ["UUID", "IsLimited", "Platform", "AppID", "IsScan", "IsRedeem", "Stamps", "CreateDate"]
		let values: [String] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"ios",
			String(id),
			String(isScan),
			String(isRedeem),
			String(stamps),
			Utils.getDateInUTC(date: Date())
		]
		
		if !UserDefaults.standard.bool(forKey: "LoyaltyTrackingDisabled") {
			GeowaveService.callBit(api: API.LoyaltyFeedback, keys: keys, values: values)
		}
	}
	
	//  class func TrackLocationSettings() {
	//
	//    Log.add(detailedLog: "API Call: Track Location Settings")
	//
	//    let keys: [String] = ["HandsetUDID", "AppID", "HasLocationEnabled", "CreateDate"]
	//    let values: [String] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      String(UserSettings.current.HasLocationEnabled),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    GeowaveService.call(api: API.LocationSettings, keys: keys, values: values)
	//
	//  }
	//
	//  class func TrackSegment(segment: String, values: [Any], delete: Bool) {
	//
	//    Log.add(detailedLog: "API Call: Track Segment (delete: \(delete))")
	//
	//    let keys: [String] = ["HandsetUDID", "AppID", "Name", "Values", "IsDeleted", "CreateDate"]
	//    let values: [Any] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      segment,
	//      values,
	//      String(delete),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    GeowaveService.call(api: API.Segment, keys: keys, values: values)
	//  }
	//
	//  class func TrackSegment(segment: String) {
	//
	//    Log.add(detailedLog: "API Call: Remove Segment")
	//
	//    let keys: [String] = ["HandsetUDID", "AppID", "Name", "IsDeleted", "CreateDate"]
	//    let values: [Any] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      segment,
	//      String(true),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    GeowaveService.call(api: API.Segment, keys: keys, values: values)
	//  }
	//
	//  class func RemoveSegments() {
	//
	//    Log.add(detailedLog: "API Call: Remove All Segments")
	//
	//    let keys: [String] = ["HandsetUDID", "AppID", "Name", "IsDeleted", "CreateDate"]
	//    let values: [String] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      "",
	//      String(true),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    GeowaveService.call(api: API.Segment, keys: keys, values: values)
	//  }
	//
	//  class func createFunnel(name: String, level: Int) {
	//
	//    Log.add(detailedLog: "API Call: Track Funnel")
	//
	//    let keys: [String] = ["HandsetUDID", "AppID", "Name", "Level", "Session", "IsDeleted", "CreateDate"]
	//    let values: [String] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      name,
	//      String(level),
	//      UserActions.current.AppOpens.currentSessionID,
	//      String(false),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    GeowaveService.call(api: API.Funnel, keys: keys, values: values)
	//
	//  }
	//
	//  class func removeAllFunnels() {
	//
	//    Log.add(detailedLog: "API Call: Remove Funnels")
	//
	//    let keys: [String] = ["HandsetUDID", "AppID", "Name", "IsDeleted", "CreateDate"]
	//    let values: [String] = [
	//      UserSettings.current.HandsetUDID,
	//      String(UserSettings.current.AppID),
	//      "",
	//      String(true),
	//      Utils.getDateInUTC(date: Date())
	//    ]
	//
	//    GeowaveService.call(api: API.Funnel, keys: keys, values: values)
	//
	//  }
	
	class func Optin(allow: Bool) {
		
		UserDefaults.standard.set(allow, forKey: "allowTracking")
		UserDefaults.standard.synchronize()
		
		Log.add(detailedLog: "API Call: Remove Segment")
		
		let keys: [String] = ["UUID", "IsLimitedAd", "AppID", "OptinOut", "PVersion", "BundleId", "Platform", "TimeStamp"]
		let values: [Any] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			//String(UserSettings.current.HandsetUDID),
			String(UserSettings.current.HasLimitedAd),
			String(UserSettings.current.AppID),
			String(allow),
			"v1 2018-01-01",
			String(Bundle.main.bundleIdentifier!),
			"ios",
			Utils.getDateInUTC(date: Date())
		]
		
		GeowaveService.callBit(api: API.Optin, keys: keys, values: values)
	}
	
	
	
	class func AuthCode(code: String) {

		UserDefaults.standard.synchronize()
		
		Log.add(detailedLog: "API Call: Remove Segment")
		
		let keys: [String] = ["UUID", "IsLimitedAd", "AppID", "Code", "TimeStamp"]
		let values: [Any] = [
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID,
			String(UserSettings.current.HasLimitedAd),
			"86219",
			String(code),
			Utils.getDateInUTC(date: Date())
		]
		
		GeowaveService.callTrans(api: API.AuthCode, keys: keys, values: values)
	}
	
	
//
//	class func createRequest(message: String, phone: String = "", isOrder: Bool, isBooking: Bool, completion: @escaping(String) -> Void )  {
//
//		Log.add(detailedLog: "API Call: Create FS Order")
//
//		let keys: [String] = ["HandsetUDID", "AppID", "IsOrder", "IsBooking", "Message", "Phone", "CreateDate"]
//		let values: [String] = [
//			UserSettings.current.HandsetUDID,
//			String(UserSettings.current.AppID),
//			String(isOrder),
//			String(isBooking),
//			message,
//			phone,
//			Utils.getDateInUTC(date: Date())
//		]
//
//		GeowaveService.createRequest(api: API.FreshstampRequest, keys: keys, values: values) { (rslt) in
//			completion(rslt)
//		}
//	}
//
//	class func createRequest(api: API, keys: [AnyHashable], values: [Any], result: @escaping(String) -> Void ) {
//
//		let request = prepareRequest(url: API.baseURL + api.rawValue, keys: keys, values: values)
//
//		let result = GeowaveService.sendPostRequestWithURL(urlRequest: request, completionHandler: { (data, response, error) in
//
//			if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
//
//				let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//				result((responseString ?? "") as String)
//			}
//		})
//		result.resume()
//	}
	
	// MARK: EXTERNAL REQUESTS
	
	private class func callBit(api: API, keys: [AnyHashable], values: [Any] ) {
		
		DispatchQueue.global(qos: DispatchQoS.QoSClass.background  ).async {
			
			let request = prepareRequest(url: API.BaseURL_BitQueen + api.rawValue, keys: keys, values: values)
			
			let result = sendPostRequestWithURL(urlRequest: request, completionHandler: { (data, response, error) in
				
				if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
					
				} else {
					Log.add(detailedLog: "api call fail: \(String(describing: error))")
					
					current.requests.append(request)
				}
				
				if data != nil {
					//let json = try? JSONSerialization.jsonObject(with: data, options: [])
				}
				
			})
			
			result.resume()
		}
		
		GeowaveService.callFailed()
	}
	
	private class func callDataReq(api: API, keys: [AnyHashable], values: [Any] ) {
		
		DispatchQueue.global(qos: DispatchQoS.QoSClass.background  ).async {
			
			let request = prepareRequest(url: API.baseURL_DataReq + api.rawValue, keys: keys, values: values)
			
			let result = sendPostRequestWithURL(urlRequest: request, completionHandler: { (data, response, error) in
				
				if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
					
				} else {
					Log.add(detailedLog: "api call fail: \(String(describing: error))")
					
					current.requests.append(request)
				}
				
				if data != nil {
					//let json = try? JSONSerialization.jsonObject(with: data, options: [])
				}
				
			})
			
			result.resume()
		}
		
		GeowaveService.callFailed()
	}
	
	private class func callTrans(api: API, keys: [AnyHashable], values: [Any] ) {
		
		DispatchQueue.global(qos: DispatchQoS.QoSClass.background  ).async {
			
			let request = prepareRequest(url: API.baseURL_Transactions + api.rawValue, keys: keys, values: values)
			
			let result = sendPostRequestWithURL(urlRequest: request, completionHandler: { (data, response, error) in
				
				if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
					
				} else {
					Log.add(detailedLog: "api call fail: \(String(describing: error))")
					
					current.requests.append(request)
				}
				
				if data != nil {
					//let json = try? JSONSerialization.jsonObject(with: data, options: [])
				}
				
			})
			
			result.resume()
		}
		
		GeowaveService.callFailed()
	}
	
	class func DataRequest(email: String) {
		
		Log.add(detailedLog: "API Call: Data Requested")
		
		let keys: [String] = ["Appid", "DataReq", "Email",  "Platform",  "TimeStamp", "Token", "Uuid"]
		let values: [String] = [
			String(UserSettings.current.AppID),
			"1",
			String(email),
			"ios",
			Utils.getDateInUTC(date: Date()),
			"tokenhaha",
			UserSettings.current.HasLimitedAd ? UserSettings.current.HandsetUDID : UserSettings.current.AdvertisingID
		]
		GeowaveService.callDataReq(api: API.DataRequest, keys: keys, values: values)
	}
	
	//  private class func call(api: API, keys: [AnyHashable], values: [Any] ) {
	//
	//    DispatchQueue.global(qos: DispatchQoS.QoSClass.background  ).async {
	//
	//      let request = prepareRequest(url: API.baseURL + api.rawValue, keys: keys, values: values)
	//
	//      let result = sendPostRequestWithURL(urlRequest: request, completionHandler: { (data, response, error) in
	//
	//        if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
	//
	//        } else {
	//          Log.add(detailedLog: "api call fail: \(String(describing: error))")
	//
	//          current.requests.append(request)
	//        }
	//
	//        if data != nil {
	//          //let json = try? JSONSerialization.jsonObject(with: data, options: [])
	//        }
	//
	//      })
	//
	//      result.resume()
	//    }
	//
	//    GeowaveService.callFailed()
	//  }
	
	private class func callFailed() {
		
		if -current.lastRetry.timeIntervalSinceNow > 15 {
			current.isRetrying = false
		}
		
		if !current.isRetrying {
			
			current.lastRetry = Date()
			current.isRetrying = true
			
			DispatchQueue.global(qos: DispatchQoS.QoSClass.background  ).async {
				
				var failedRequests = [URLRequest]()
				
				Utils().synchronize(lockObj: current.requests as AnyObject, closure: {
					for request in current.requests {
						failedRequests.append(request)
					}
					current.requests = [URLRequest]()
				})
				
				if failedRequests.count > 0 {
					
					Log.add(detailedLog: "Failed API's to retry: \(failedRequests.count)")
					
					for request in failedRequests {
						
						Log.add(detailedLog: "Retrying api call now!...")
						
						let result = sendPostRequestWithURL(urlRequest: request, completionHandler: { (data, response, error) in
							
							if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
								Log.add(detailedLog: "Retry OK")
							} else {
								current.requests.append(request)
								Log.add(detailedLog: "Retry FAIL")
							}
							
							if data != nil {
								//let json = try? JSONSerialization.jsonObject(with: data, options: [])
							}
							
							current.lastRetry = Date()
						})
						
						result.resume()
					}
					current.isRetrying = false
				}
			}
		}
	}
	
	private class func callExternalAPI(api: API, keys:[String], values:[String]) {
		
		DispatchQueue.global(qos: DispatchQoS.QoSClass.background ).async {
			
			let request = prepareExternalRequest(url: api.rawValue, keys: keys, values: values)
			
			let result = sendRequest(urlRequest: request, completionHandler: { (data, response, error) in
				
				if let response = response as? HTTPURLResponse , 200...299 ~= response.statusCode {
					
				} else {
					Log.add(warning: "External API Call: Failed - \(String(describing: error))")
				}
				
				if data != nil {
					//let json = try? JSONSerialization.jsonObject(with: data, options: [])
				}
				
			})
			
			result.resume()
		}
	}
	
	class func sendPostRequestWithURL(urlRequest: URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionTask {
		
		let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: completionHandler)
		
		task.resume()
		
		return task
		
	}
	
	class func prepareRequest(url: String, keys:[AnyHashable], values:[Any], isAuthorized: Bool = false) -> URLRequest {
		var bodyData = [AnyHashable: Any]()
		
		for (index, element) in keys.enumerated() {
			bodyData[element] = values[index]
		}
		
		var urlRequest = URLRequest.init(url: URL.init(string: url)!, cachePolicy: URLRequest.CachePolicy.reloadIgnoringLocalCacheData, timeoutInterval: 10)
		urlRequest.httpMethod = "POST"
		urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
		
		if isAuthorized {
			urlRequest.setValue(Geowave.AUTHORIZATION_KEY, forHTTPHeaderField: "Authorization-Token")
		}
		
		do {
			
			let jsonData = try JSONSerialization.data(withJSONObject: bodyData, options: .prettyPrinted)
			
			urlRequest.httpBody = jsonData
			
		} catch {
			Log.add(warning: "API Call: Error parsing json")
		}
		
		return urlRequest
	}
	
	private class func prepareExternalRequest(url: String, keys:[String], values:[String]) -> URLRequest {
		
		var params = [String: String]()
		
		for (index, element) in keys.enumerated() {
			params[element] = values[index]
		}
		
		let parameterString = params.stringFromHttpParameters()
		let requestURL = URL(string:"\(url)?\(parameterString)")!
		
		var request = URLRequest(url: requestURL)
		request.httpMethod = "GET"
		
		return request
	}
	
	private class func sendRequest(urlRequest :URLRequest, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionTask {
		
		let task = URLSession.shared.dataTask(with: urlRequest, completionHandler: completionHandler)
		task.resume()
		
		return task
	}
	
}
																	
