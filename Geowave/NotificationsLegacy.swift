//
//  UserNotifications
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import UIKit

class NotificationsLegacy : NSObject {

  // MARK: Notifications Legacy INIT

  static let current : NotificationsLegacy = {

    let current = NotificationsLegacy()
    
    return current

  }()

  // MARK: Schedule Notifications

  class func scheduleLocalNotification(title: String, message: String) {
    
    //TODO: implement with iOS9 methods

  }

  // MARK: Notifications Callbacks

  class func didRegisterForRemoteNotifications(deviceToken: Data) {
    
    let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
    
    UserSettings.set(DeviceToken: deviceTokenString)
    UserSettings.set(HasPushEnabled: UIApplication.shared.isRegisteredForRemoteNotifications)
    
  }
  
  class func requestNotificationAuthorization() {
    
		UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
    UIApplication.shared.registerForRemoteNotifications()

  }
  
  class func didReceiveRemoteNotification(userInfo: [AnyHashable : Any]) -> UIBackgroundFetchResult {
    
    Log.add(log: "Did Receive Notification")

    switch(UIApplication.shared.applicationState) {

		case UIApplication.State.active:
      Log.add(log: "Notification opened in foreground")
      return UIBackgroundFetchResult.newData

    case UIApplication.State.inactive:
      Log.add(log: "Notification opened in background")
      NotificationsShared.process(userInfo: userInfo)
      return UIBackgroundFetchResult.newData

    case UIApplication.State.background:
      Log.add(log: "Notification received in background")
      NotificationsShared.process(userInfo: userInfo)
      return UIBackgroundFetchResult.newData
    }

  }

  class func didFailToRegisterForRemoteNotifications(error: Error) {
    
    UserSettings.set(HasPushEnabled: UIApplication.shared.isRegisteredForRemoteNotifications)
    
    Log.add(log: "Error getting Notifications Authorization: \(error)")
    
  }
  
}
