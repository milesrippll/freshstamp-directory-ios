//
//  UserLocation
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

class UserLocation {
	
	// MARK: declarations
	
	static let current : UserLocation = {
		let current = UserLocation()
		return current
		
	}()
	
	class func setupBeacons() {
		
		// setup beacon monitoring
		if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
			
			if CLLocationManager.isRangingAvailable() {
				
				for monitored in (Location.current.LocationManager.monitoredRegions) {
					Location.current.LocationManager.stopMonitoring(for: monitored)
				}
				
				let uuid = UUID.init(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!
				let major = CLBeaconMajorValue(3043)
				let minor = CLBeaconMinorValue(43503)
				let identifier = "rippllbeacon"
				let beaconRegion = CLBeaconRegion.init(proximityUUID: uuid, major: major, minor: minor, identifier: identifier)
				
				beaconRegion.notifyOnEntry = true
				beaconRegion.notifyOnExit = true
				beaconRegion.notifyEntryStateOnDisplay = true
				
				Location.current.LocationManager.startMonitoring(for: beaconRegion)
				
				//				let fenceRegion = CLCircularRegion.init(center: CLLocationCoordinate2D.init(latitude: b.lat, longitude: b.lon), radius: 50.0, identifier: "\(b.identifier) + fence")
				//				fenceRegion.notifyOnEntry = true
				//				fenceRegion.notifyOnExit = true
				//
				//				Location.current.LocationManager.startMonitoring(for: fenceRegion)
				
			}
		}
	}
}


