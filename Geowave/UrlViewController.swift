//
//  WebViewController.swift
//  geowave
//
//  Created by Doug Chisholm on 22/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import UIKit

class UrlViewController: UINavigationController, UIWebViewDelegate {
	
	let vc = UIViewController()
	var campaignID: Int = 0
	
	convenience init(image: String, title: String, and color: UIColor) {
		
		self.init()
		
		vc.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: title, style: UIBarButtonItemStyle.done, target: self, action: #selector(UrlViewController.close))
		vc.navigationItem.rightBarButtonItem?.tintColor = color
		
		if (image != "") {
			
			let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 40))
			
			let titleImageView = UIImageView(image: UIImage(named: image))
			titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)
			
			titleView.addSubview(titleImageView)
			
			vc.navigationItem.titleView = titleView
			
		}
		
		self.pushViewController(vc, animated: true)
	}
	
	func showUrl(url: String, campaignID: Int) {
		
		self.campaignID = campaignID
		
		let webVw = UIWebView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
		webVw.delegate = self
		webVw.scalesPageToFit = true
		webVw.scrollView.bounces = false
		webVw.loadRequest(URLRequest.init(url: URL.init(string: url)!))
		
		let activityView : UIActivityIndicatorView = UIActivityIndicatorView.init(activityIndicatorStyle: .gray)
		activityView.center = CGPoint(x: self.view.frame.size.width/2 , y: self.view.frame.size.height/2)
		activityView.startAnimating()
		activityView.tag = 100;
		self.view.addSubview(activityView)
		
		let refreshControl : UIRefreshControl = UIRefreshControl.init()
		refreshControl.addTarget(self, action: #selector(UrlViewController.handleRefresh(_:)), for: .valueChanged)
		
		webVw.scrollView.addSubview(refreshControl)
		
		vc.view.addSubview(webVw)
		
	}
	
	override func viewWillAppear(_ animated: Bool) {
		
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
	@objc func close() {
		
		//GeowaveService.TrackPushFeedback(campaignID: self.campaignID)
		self.dismiss(animated: true, completion: nil)
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	func webViewDidFinishLoad(_ webView: UIWebView) {
		
		self.view.viewWithTag(100)?.isHidden = true
	}
	
	func webViewDidStartLoad(_ webView: UIWebView) {
	}
	
	override var shouldAutorotate: Bool {
		return false
	}
	
	override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		return UIInterfaceOrientationMask.portrait
	}
	
	
	func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
		
		return true
	}
	
	@objc func handleRefresh(_ refresh: UIRefreshControl)
	{
		refresh.endRefreshing()
	}
}
