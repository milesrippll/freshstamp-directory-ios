//
//  LocationModel.swift
//  geowave
//
//  Created by Doug Chisholm on 29/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct POI {

  var reference: String = ""
  var location: GWLocation = GWLocation()
  var shadows: [GWLocation] = []
  
}

struct LocationVisit {

  var Visit: CLVisit
  var Locations: [CLLocation]
  var ConnectionStatus: Reachability.NetworkStatus
  var IsCurrent: Bool

  init(visit: CLVisit) {
    Visit = visit
    Locations = []
    ConnectionStatus = Reachability.NetworkStatus.notReachable
    IsCurrent = false
  }

  var duration: Double {
    return Visit.departureDate.timeIntervalSince(Visit.arrivalDate)
  }

  var isCurrent: Bool {
    return Date.init() > Visit.arrivalDate && Date.init() < Visit.departureDate
  }

}

