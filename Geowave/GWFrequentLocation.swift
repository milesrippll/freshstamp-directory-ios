//
//  GeowaveTypes.swift
//  freshstamp
//
//  Created by Doug Chisholm on 11/01/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct DwelveTime {
  var arrivalTimestamp: Date
  var departureTimestamp: Date

  init() {
    self.arrivalTimestamp = Date.distantPast
    self.departureTimestamp = Date.distantFuture
  }
}

extension DwelveTime {

  init(arrival: Date, departure: Date) {
    self.arrivalTimestamp = arrival
    self.departureTimestamp = departure
  }

  init(_ CLVisit: CLVisit) {
    self.init(arrival: CLVisit.arrivalDate, departure: CLVisit.departureDate)
  }
}

struct GWFrequentLocation {

  var location : GWLocation
  var dwelveTime: DwelveTime

  init() {
    self.location = GWLocation()
    self.dwelveTime = DwelveTime()
  }
}

extension GWFrequentLocation {

  init(location: GWLocation, dwelveTime: DwelveTime) {
    self.location = location
    self.dwelveTime = dwelveTime
  }
  
  init(_ CLVisit: CLVisit) {
    self.init(location: GWLocation(CLVisit), dwelveTime: DwelveTime(CLVisit))
  }
}
