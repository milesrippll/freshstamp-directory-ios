//
//  AppOpenStore.swift
//  geowave
//
//  Created by Doug Chisholm on 29/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct AppOpenStore {

  // MARK: INIT

  init() {
    AppOpens = []
  }

  // MARK : ATTRIBUTES

  var AppOpens: [AppOpen]

  var hasAppOpens: Bool {

    guard AppOpens.count > 0 else {
      return false
    }
    return true
  }

  var lastOpenIndex: Int? {
    guard hasAppOpens else {
      return nil
    }
    return AppOpens.endIndex - 1
  }

  var last: AppOpen? {
    guard hasAppOpens else {
      return nil
    }
    return AppOpens.last
  }

  var currentSessionID: String {

    guard hasAppOpens else {
      return ""
    }
    return (AppOpens.last?.Session)!
  }

  var currentLocation: CLLocation? {

    guard hasAppOpens else {
      return nil
    }

    return Utils.getBestLocation(locations: (AppOpens.last?.Locations)!)
  }

  var currentSessionTime: Double? {

    guard hasAppOpens else {
      return nil
    }
    return AppOpens.last?.sessionTime
  }

  mutating func add(open: Date, networkState: Reachability.NetworkStatus) {
    AppOpens.append(AppOpen(date: open))

    if let index = lastOpenIndex {
      AppOpens[index].ConnectionStatus = networkState
    }
  }

  mutating func add(close: Date) {

    if let index = lastOpenIndex {
      AppOpens[index].CloseDate = close
    }
  }

  mutating func add(location: CLLocation) {

    if let index = lastOpenIndex {

      AppOpens[index].Locations.append(location)
    }
  }
}
