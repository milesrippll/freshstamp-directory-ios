//
//  AppOpen.swift
//  geowave
//
//  Created by Doug Chisholm on 29/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct AppOpen {

  var OpenDate: Date
  var CloseDate: Date
  var Locations: [CLLocation]
  var ConnectionStatus: Reachability.NetworkStatus
  var Session: String
  var IsCurrent: Bool

  init(date: Date) {
    OpenDate = date
    CloseDate = date
    Locations = []
    ConnectionStatus = Reachability.NetworkStatus.notReachable
    Session = UUID.init().uuidString;
    IsCurrent = true
  }

  var sessionTime: Double {
    return CloseDate.timeIntervalSince(OpenDate)
  }

  var isOpen: Bool {
    return CloseDate == OpenDate
  }
}
