//
//  Utils.swift
//  geowave
//
//  Created by Doug Chisholm on 25/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

class Utils {

  // MARK: LOCATION

  class func intersects(location: CLLocation, visit: CLVisit) -> Bool {

    return intersects(latitude_1: location.coordinate.latitude, longitude_1: location.coordinate.longitude, radius_1: location.horizontalAccuracy, latitude_2: visit.coordinate.latitude, longitude_2: visit.coordinate.longitude, radius_2: visit.horizontalAccuracy)

  }

  class func intersects(location: GWLocation, visit: CLVisit) -> Bool {

    return intersects(latitude_1: location.lat, longitude_1: location.lon, radius_1: location.hAcc, latitude_2: visit.coordinate.latitude, longitude_2: visit.coordinate.longitude, radius_2: visit.horizontalAccuracy)
    
  }

  class private func intersects(latitude_1: Double, longitude_1: Double, radius_1: Double, latitude_2: Double, longitude_2: Double, radius_2: Double) -> Bool {

    let locA = CLLocation.init(latitude: latitude_1, longitude: longitude_1)
    let locB = CLLocation.init(latitude: latitude_2, longitude: longitude_2)

    let distance = locA.distance(from: locB)
    let minDistance = radius_1 + radius_2

    return distance < minDistance

  }

  class func isValidLocation(location: CLLocation) -> Bool {
    let locationAge = -location.timestamp.timeIntervalSinceNow

    if (locationAge > 5.0) {
      return false
    }
    
    if (0 > location.horizontalAccuracy || location.horizontalAccuracy > 300 ) {
      return false
    }

    return true
  }

  class func getBestLocation(locations: [CLLocation]) -> CLLocation? {

    if locations.count > 0 {
      return locations.sorted(by: { $0.horizontalAccuracy <= $1.horizontalAccuracy}).first!
    }
    return nil
  }

  // MARK: GCD

  class func delay(_ delay:Double, closure:@escaping ()->()) {
    let when = DispatchTime.now() + delay
    DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
  }

  // MARK: DateTime

  class func getDateInUTC(date: Date) -> String {

    let formatter = DateFormatter()
    formatter.calendar = Calendar(identifier: .iso8601)
    formatter.locale = Locale(identifier: "en_US_POSIX")
    formatter.timeZone = TimeZone(secondsFromGMT: 0)
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"

    return formatter.string(from: date)
  }

  // Multithread

  func synchronize(lockObj: AnyObject!, closure: ()->()){
    objc_sync_enter(lockObj)
    closure()
    objc_sync_exit(lockObj)
  }

}
