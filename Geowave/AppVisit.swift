//
//  AppVisit.swift
//  geowave
//
//  Created by Doug Chisholm on 04/11/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct AppVisit {

  var Visit: CLVisit
  var Locations: [CLLocation]
  var ConnectionStatus: Reachability.NetworkStatus
  var IsCurrent: Bool
  
  init(visit: CLVisit) {
    Visit = visit
    Locations = []
    ConnectionStatus = Reachability.NetworkStatus.notReachable
    IsCurrent = true
  }

}
