//
//  User Actions
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

class UserActions {

  // MARK: INIT

  static let current : UserActions = {

    let current = UserActions()

    return current
  }()

  var AppOpens = AppOpenStore()
  var AppVisits = AppVisitStore()
  
  //var LocationVisits: [LocationVisit] = []

  // MARK: Actions

  class func add(open: Date) {

    Log.add(log: "App Opened @\(open)")

    current.AppOpens.add(open: open, networkState: UserSettings.getNetworkState())

    Location.start()
  }

  class func add(close: Date) {

    Log.add(log: "App Closed @\(close)")

    current.AppOpens.add(close: close)

    Location.stop()

    if UserSettings.registrationIsOutdated() {

      UserSettings.fullRegistration()
    }

    if let lastOpen = current.AppOpens.last {
      GeowaveService.TrackAppOpen(appOpen: lastOpen)

      if let bestLocation = Utils.getBestLocation(locations: lastOpen.Locations) {
        GeowaveService.TrackLocation(location: bestLocation)
      }
    }
  }

  class func add(visit: CLVisit) {

    Log.add(detailedLog: "Location visited @\(visit)")

    let isArrival = visit.arrivalDate != Date.distantPast
    let isDeparture = visit.departureDate != Date.distantFuture

    //Only arrival or Only Departure -> Add to array
    if (isArrival && !isDeparture) {

      Log.add(detailedLog: "Arrived")

      current.AppVisits.add(arrival: visit)
      Location.start()

      //LocationActions.checkArrival(visit: visit)
    }

    if (isArrival && isDeparture) {

      Log.add(detailedLog: "Departed")

      current.AppVisits.add(departure: visit)

      //LocationActions.checkDeparture(visit: visit)
    }

    GeowaveService.TrackFrequentLocation(visit: visit)
    GeowaveService.TrackLocation(location: CLLocation.init(coordinate: visit.coordinate, altitude: 0, horizontalAccuracy: visit.horizontalAccuracy, verticalAccuracy: 0, timestamp: Date()))
  }

  class func add(locations: [CLLocation]) {

    if let bestLocation = Utils.getBestLocation(locations: locations) {

      if Utils.isValidLocation(location: bestLocation) {

        current.AppOpens.add(location: bestLocation)
        current.AppVisits.add(location: bestLocation)

        if !(current.AppOpens.last?.isOpen)! {
          GeowaveService.TrackLocation(location: bestLocation)
        }
      }
    }
  }
}


