//
//  UserNotifications
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

@available(iOS 10, *)
class Notifications : NSObject {

  // MARK: Notifications INIT

  var UserNotificationCenter: UNUserNotificationCenter?
  
  static let current : Notifications = {

    let current = Notifications()
    current.UserNotificationCenter = UNUserNotificationCenter.current()
    
    return current

  }()

  // MARK: Schedule Notifications

  class func scheduleLocalNotification(title: String, message: String, identifier: String = "GWLocalNotif", delay: TimeInterval) {

    let content = UNMutableNotificationContent()
    content.title = NSString.localizedUserNotificationString(forKey: title, arguments: nil)
    content.body = NSString.localizedUserNotificationString(forKey: message, arguments: nil)
		content.sound = UNNotificationSound.default()

    // Deliver the notification immediately
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: delay, repeats: false)
    let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
    
    // Schedule the notification.
    current.UserNotificationCenter?.add(request, withCompletionHandler: { (error) in

        if let e = error {
          Log.add(warning: "Error scheduling local notification: \(e)")
        }
    })
 }

  // MARK: Notifications Callbacks

  class func didRegisterForRemoteNotifications(deviceToken: Data) {
    
    let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
    
    UserSettings.set(DeviceToken: deviceTokenString)
    UserSettings.set(HasPushEnabled: UIApplication.shared.isRegisteredForRemoteNotifications)
    
  }
  
  class func requestNotificationAuthorization() {
    
      current.UserNotificationCenter?.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in

        DispatchQueue.main.sync(execute: {
          UserSettings.set(HasPushEnabled: UIApplication.shared.isRegisteredForRemoteNotifications)

          if(!granted){
            if let e = error {
              Log.add(log: "Error getting Notifications Authorization: \(e)")
            }
          }

          UIApplication.shared.registerForRemoteNotifications()
        })
      }
  }
  
  class func didReceiveNotificationResponse(response: UNNotificationResponse) {
    
    Log.add(log: "Did Receive Notification")
    
    switch(UIApplication.shared.applicationState){

    case UIApplication.State.active:
      Log.add(log: "Notification opened in foreground")
      NotificationsShared.process(userInfo: response.notification.request.content.userInfo)
      
		case UIApplication.State.inactive:
      Log.add(log: "Notification opened in background")

      NotificationsShared.process(userInfo: response.notification.request.content.userInfo)
      
    case UIApplication.State.background:
      Log.add(log: "Notification received in background")
    }

  }

  class func willPresentNotification(notification: UNNotification) -> UNNotificationPresentationOptions {

    return .alert
  }
  
  class func didFailToRegisterForRemoteNotifications(error: Error) {
    
    UserSettings.set(HasPushEnabled: UIApplication.shared.isRegisteredForRemoteNotifications)
    
    Log.add(log: "Error getting Notifications Authorization: \(error)")

  }

}
