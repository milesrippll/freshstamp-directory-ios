//
//  Geowave
//
//  Created by Doug Chisholm on 18/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import UserNotifications
import UIKit

public class Geowave {
	
	// MARK: Internal Properties
	
	static let CURR_SDK_VERSION = "6.0.0"
	static let AUTHORIZATION_KEY = "F4B7E0D2-CC77-47F9-BE9E-2B1DEA6F2A6A"
	
	static let isSimulator: Bool = {
		var isSim = false
		#if arch(i386) || arch(x86_64)
		isSim = true
		#endif
		return isSim
	}()
	
	var isReady: Bool = false
	var urlView: UrlViewController = UrlViewController()
	
	// MARK: Geowave INIT
	static let current : Geowave = {
		let current = Geowave()
		return current
	}()
	
	//Starts the SDK without any logs
	public class func start() {
		
		Log.current.Styles = [LogStyle.None]
		sdkStart()
	}
	
	//Starts the SDK with specified log styles
	//  public class func start(withLogStyles: [LogStyle]) {
	//
	//    Log.current.Styles = withLogStyles
	//    sdkStart()
	//
	//  }
	
	public class func setWebView(logo: String, close: String, color: UIColor) {
		current.urlView = UrlViewController.init(image: logo, title: close, and: color)
	}
	
	//SDK Start
	private class func sdkStart() {
		
		//add app delegate callbacks
		addAppDelegateCallbacks()
		
		//loads all settings and notifies any changes
		UserSettings.load()
		
		//Sets the SDK Version
		UserSettings.set(SDKVersion: CURR_SDK_VERSION)
		
		// Updates the device name
		UserSettings.set(DeviceName: UIDevice.current.name )
		
		//saves current settings
		UserSettings.save()
		
		UserSettings.updateChanges()
		
		//UserLocation.setupBeacons()
		
		if(!UserSettings.valid()) {
			Log.add(warning: "Geowave SDK NOT READY")
		}
		else {
			current.isReady = true
			Log.add(log: "Geowave SDK Started...")
			appOpened()
		}
		
	}
	
	//  public class var currentLog: String {
	//    return Log.current.fullLog
	//  }
	
	// MARK: Request Authorizations
	
	public class func requestLocationAlwaysAuthorization() {
		
		if(!current.isReady){
			Log.add(warning: "Geowave SDK NOT READY")
		}
		else {
			Location.request(with: .Always)
		}
	}
	
	public class func requestLocationWhenInUseAuthorization() {
		
		if(!current.isReady){
			Log.add(warning: "Geowave SDK NOT READY")
		}
		else {
			Location.request(with: .WhenInUse)
		}
	}
	
	public class func requestNotificationAuthorization() {
		
		if #available(iOS 10, *) {
			
			Notifications.requestNotificationAuthorization()
			
		} else {
			
			NotificationsLegacy.requestNotificationAuthorization()
			
		}
	}
	
	// MARK: Delegate Callbacks
	
	class func addAppDelegateCallbacks() {
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(appClosed),
			name: .UIApplicationDidEnterBackground,
			object: nil)
		
		NotificationCenter.default.addObserver(
			self,
			selector: #selector(appOpened),
			name: .UIApplicationWillEnterForeground,
			object: nil)
	}
	
	public class func didRegisterForRemoteNotifications(deviceToken: Data) {
		
		if #available(iOS 10, *) {
			
			Notifications.didRegisterForRemoteNotifications(deviceToken: deviceToken)
			
		} else {
			
			NotificationsLegacy.didRegisterForRemoteNotifications(deviceToken: deviceToken)
		}
	}
	
	public class func didFailToRegisterForRemoteNotifications(error: Error) {
		
		if #available(iOS 10, *) {
			
			Notifications.didFailToRegisterForRemoteNotifications(error: error)
			
		} else {
			
			NotificationsLegacy.didFailToRegisterForRemoteNotifications(error: error)
		}
	}
	
	@available(iOS 10, *)
	public class func didReceiveNotificationResponse(response: UNNotificationResponse) {
		
		Notifications.didReceiveNotificationResponse(response: response)
		
	}
	
	@available(iOS 9, *)
	public class func applicationDidReceiveRemoteNotification(userInfo: [AnyHashable : Any]) -> UIBackgroundFetchResult {
		
		return NotificationsLegacy.didReceiveRemoteNotification(userInfo: userInfo)
		
	}
	
	@available(iOS 10, *)
	public class func willPresentNotification(notification: UNNotification) -> UNNotificationPresentationOptions {
		
		return Notifications.willPresentNotification(notification: notification)
		
	}
	
	// MARK: Local Notifications
	
	
	public class func scheduleLocalNotification(title: String, message: String, identifier: String = "GWLocalNotif", delay: TimeInterval) {
		
		if #available(iOS 10, *) {
			
			Notifications.scheduleLocalNotification(title: title, message: message, identifier: identifier, delay: delay)
			
		} else {
			
			NotificationsLegacy.scheduleLocalNotification(title: title, message: message)
		}
	}
	
	// MARK: AppOpen/Closed Actions
	
	@objc class func appClosed() {
		
		if(!current.isReady){
			
			Log.add(warning: "Geowave SDK NOT READY")
			
		} else {
			
			UserSettings.updateChanges()
			UserActions.add(close: Date.init())
		}
	}
	
	@objc class func appOpened() {
		
		if(!current.isReady){
			
			Log.add(warning: "Geowave SDK NOT READY")
			
		} else {
			
			UserSettings.updateChanges()
			UserActions.add(open: Date.init())
		}
	}
	
	// MARK: User Segments
	
	//  public class func createSegmentsWithName(name: String, values: [Any]) {
	//
	//    GeowaveService.TrackSegment(segment: name, values: values, delete: false)
	//  }
	//
	//  public class func removeSegmentsWithName(name: String, values: [Any]) {
	//
	//    GeowaveService.TrackSegment(segment: name, values: values, delete: true)
	//  }
	//
	//  public class func removeSegmentsWithName(name: String) {
	//
	//    GeowaveService.TrackSegment(segment: name)
	//  }
	//
	//  public class func removeAllSegments() {
	//
	//    GeowaveService.RemoveSegments()
	//  }
	//
	//  public class func createFunnelWithName(name: String, level: Int) {
	//
	//    GeowaveService.createFunnel(name: name, level: level)
	//  }
	
	//  public class func removeAllFunnels() {
	//
	//    GeowaveService.removeAllFunnels()
	//  }
//
//	public class func CreateOrder(message: String, completion: @escaping (String) -> Void ) {
//		GeowaveService.createRequest(message: message, isOrder: true, isBooking: false) { (rslt) in
//			completion(rslt)
//		}
//	}
//
//	public class func CreateBooking(message: String, phone: String, completion: @escaping (String) -> Void ) {
//		GeowaveService.createRequest(message: message, phone: phone, isOrder: false, isBooking: true) { (rslt) in
//			completion(rslt)
//		}
//	}
}
