//
//  GeowaveTypes.swift
//  freshstamp
//
//  Created by Doug Chisholm on 11/01/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct GWLocation {

  var lat: Double
  var lon: Double
  var alt: Double
  var hAcc: Double
  var vAcc: Double
  var timestamp: Date

  init() {
    self.lat = 0.0
    self.lon = 0.0
    self.alt = 0.0
    self.vAcc = 0.0
    self.hAcc = 0.0
    self.timestamp = Date()
  }

  init(lat: Double, lon: Double, vAcc: Double) {
    self.lat = lat
    self.lon = lon
    self.alt = 0.0
    self.vAcc = vAcc
    self.hAcc = 0.0
    self.timestamp = Date()
  }
}

// adds init from CLLocation
extension GWLocation {

  init(lat: Double, lon: Double, alt: Double, vAcc: Double, hAcc: Double, timestamp: Date) {
    self.lat = lat
    self.lon = lon
    self.alt = alt
    self.vAcc = vAcc
    self.hAcc = hAcc
    self.timestamp = timestamp
  }

  init(_ CLLocation: CLLocation) {
    self.init(lat: CLLocation.coordinate.latitude, lon: CLLocation.coordinate.longitude, alt: CLLocation.altitude, vAcc: CLLocation.verticalAccuracy, hAcc: CLLocation.horizontalAccuracy, timestamp: CLLocation.timestamp)
  }
}

// adds init from CLVisit
extension GWLocation {

  init(lat: Double, lon: Double, timestamp: Date) {
    self.lat = lat
    self.lon = lon
    self.alt = 0.0
    self.vAcc = 0.0
    self.hAcc = 0.0
    self.timestamp = timestamp
  }

  init(_ CLVisit: CLVisit) {
    self.init(lat: CLVisit.coordinate.latitude, lon: CLVisit.coordinate.longitude, timestamp: CLVisit.arrivalDate)
  }
}
