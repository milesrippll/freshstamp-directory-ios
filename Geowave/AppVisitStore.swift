//
//  AppOpenStore.swift
//  geowave
//
//  Created by Doug Chisholm on 29/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import CoreLocation

struct AppVisitStore {

  // MARK: INIT

  init() {
    AppVisits = []
  }

  // MARK : ATTRIBUTES

  var AppVisits: [AppVisit]

  var hasAppVisits: Bool {

    guard AppVisits.count > 0 else {
      return false
    }
    return true
  }

  var lastVisitIndex: Int? {
    guard hasAppVisits else {
      return nil
    }
    return AppVisits.endIndex - 1
  }

  var last: AppVisit? {
    return AppVisits.last
  }

  var currentLocation: CLLocation? {

    if let index = lastVisitIndex {
      return Utils.getBestLocation(locations: AppVisits[index].Locations)
    }
    return nil
  }

  mutating func add(arrival: CLVisit) {
    AppVisits.append(AppVisit(visit: arrival))
  }

  mutating func add(departure: CLVisit) {

    if let index = AppVisits.index(where: { $0.Visit.arrivalDate == departure.arrivalDate && $0.Visit.departureDate == Date.distantFuture }) {
      AppVisits[index].Visit = departure
    }
  }

  mutating func add(location: CLLocation) {

    if let index = lastVisitIndex {

      if Utils.intersects(location: location, visit: AppVisits[index].Visit) {
        if (AppVisits[index].Visit.arrivalDate...AppVisits[index].Visit.departureDate).contains(location.timestamp) {
          AppVisits[index].Locations.append(location)
        }
      }
    }
  }
}
