//
//  Extensions.swift
//  geowave
//
//  Created by Doug Chisholm on 28/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation

extension String {

  /// Percent escapes values to be added to a URL query as specified in RFC 3986
  ///
  /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
  ///
  /// http://www.ietf.org/rfc/rfc3986.txt
  ///
  /// :returns: Returns percent-escaped string.

  func addingPercentEncodingForURLQueryValue() -> String? {
    let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")

    return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
  }

}

extension Dictionary {

  /// Build string representation of HTTP parameter dictionary of keys and objects
  ///
  /// This percent escapes in compliance with RFC 3986
  ///
  /// http://www.ietf.org/rfc/rfc3986.txt
  ///
  /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped

  func stringFromHttpParameters() -> String {
    let parameterArray = self.map { (key, value) -> String in
      let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
      let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
      return "\(percentEscapedKey)=\(percentEscapedValue)"
    }

    return parameterArray.joined(separator: "&")
  }

}

extension Bundle {

  var releaseVersionNumber: String? {
    return self.infoDictionary?["CFBundleShortVersionString"] as? String
  }

  var buildVersionNumber: String? {
    return self.infoDictionary?["CFBundleVersion"] as? String
  }
  
}
