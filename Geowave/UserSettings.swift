//
//  UserSettings
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation
import UIKit
import AdSupport

class UserSettings {
	
	// MARK: INIT
	
	static let current : UserSettings = {
		let current = UserSettings()
		return current
	}()
	
	var AppID: Int = 0
	var HandsetUDID: String = ""
	var AdvertisingID: String = ""
	var HasLimitedAd: Bool = false
	var HasPushEnabled: Bool = false
	var HasLocationEnabled: Bool = false
	var DeviceModel: String = ""
	var SystemVersion: String = ""
	var SDKVersion: String = ""
	var AppVersion: String = ""
	var DeviceToken: String = ""
	var DeviceName: String = ""
	//var GeowaveBeacons: Data?
	
	var hasChanges: Bool = false
	
	var LastRegisterDevice: Date = Date()
	var LastBeaconRefresh: Date = Date()
	var LastBeaconEnterRegion: Date = Date()
	var CurrentCity: String = ""
	
	let MAX_TIME_BETWEEN_REGISTRATION: Double = 60 * 60 * 24 * 3 //3 DAYS
	let MAX_TIME_BETWEEN_BEACON_REFRESH: Double = 60 * 60 * 24 * 3 //3 DAYS
	let MAX_TIME_BETWEEN_BEACON_ALERT: Double = 60 * 60 //60 MINUTE
	
	let reachability = Reachability()!
	
	// MARK: LOAD/SAVE/UPDATE
	
	class func load() {
		
		let defaults = UserDefaults.standard
		
		//load current values
		current.AppID = current.getAppID()
		current.HandsetUDID = (UIDevice.current.identifierForVendor?.uuidString)!
		current.AdvertisingID = ASIdentifierManager.shared().advertisingIdentifier.uuidString
		current.HasLimitedAd = !ASIdentifierManager.shared().isAdvertisingTrackingEnabled
		current.HasPushEnabled = UIApplication.shared.isRegisteredForRemoteNotifications
		current.DeviceModel = UIDevice.current.model
		current.SystemVersion = UIDevice.current.systemVersion
		current.AppVersion = Bundle.main.releaseVersionNumber!
		
		//create defaults if not exists (only string defaults)
		if defaults.string(forKey: Keys.HandsetUDID.rawValue) == nil {
			defaults.set("", forKey: Keys.HandsetUDID.rawValue)
		}
		
		if defaults.string(forKey: Keys.AdvertisingID.rawValue) == nil {
			defaults.set("", forKey: Keys.AdvertisingID.rawValue)
		}
		
		if defaults.string(forKey: Keys.DeviceModel.rawValue) == nil {
			defaults.set("", forKey: Keys.DeviceModel.rawValue)
		}
		
		if defaults.string(forKey: Keys.SystemVersion.rawValue) == nil {
			defaults.set("", forKey: Keys.SystemVersion.rawValue)
		}
		
		if defaults.string(forKey: Keys.AppVersion.rawValue) == nil {
			defaults.set("", forKey: Keys.AppVersion.rawValue)
		}
		
		if defaults.string(forKey: Keys.DeviceToken.rawValue) == nil {
			defaults.set("", forKey: Keys.DeviceToken.rawValue)
		}
		
		if defaults.string(forKey: Keys.DeviceName.rawValue) == nil {
			defaults.set("", forKey: Keys.DeviceName.rawValue)
		}
		
		if defaults.object(forKey: Keys.LastRegisterDevice.rawValue) == nil {
			defaults.set(Date(), forKey: Keys.LastRegisterDevice.rawValue)
		}
		
		if defaults.object(forKey: Keys.LastBeaconRefresh.rawValue) == nil {
			defaults.set(Date.distantPast, forKey: Keys.LastBeaconRefresh.rawValue)
		}
		
		if defaults.object(forKey: Keys.LastBeaconEnterRegion.rawValue) == nil {
			defaults.set(Date.distantPast, forKey: Keys.LastBeaconEnterRegion.rawValue)
		}
		
		if defaults.string(forKey: Keys.CurrentCity.rawValue) == nil {
			defaults.set("", forKey: Keys.CurrentCity.rawValue)
		}
		
		//    if defaults.object(forKey: Keys.GeowaveBeacons.rawValue) == nil {
		//      defaults.set([GeowaveBeacon](), forKey: Keys.GeowaveBeacons.rawValue)
		//    }
		
		//load remaining user settings
		current.HasLocationEnabled = defaults.bool(forKey: Keys.HasLocationEnabled.rawValue)
		current.hasChanges = defaults.bool(forKey: Keys.HasChanges.rawValue)
		current.DeviceToken = defaults.string(forKey: Keys.DeviceToken.rawValue) ?? ""
		current.DeviceName = defaults.string(forKey: Keys.DeviceName.rawValue) ?? ""
		current.LastRegisterDevice = defaults.object(forKey: Keys.LastRegisterDevice.rawValue) as? Date ?? Date()
		current.LastBeaconRefresh = defaults.object(forKey: Keys.LastBeaconRefresh.rawValue) as? Date ?? Date()
		current.LastBeaconEnterRegion = defaults.object(forKey: Keys.LastBeaconEnterRegion.rawValue) as? Date ?? Date()
		current.CurrentCity = defaults.string(forKey: Keys.CurrentCity.rawValue) ?? ""
		//current.GeowaveBeacons = defaults.data(forKey: Keys.GeowaveBeacons.rawValue)
		
	}
	
	class func save() {
		
		let defaults = UserDefaults.standard
		
		//update defaults if different from settings
		if current.AppID != defaults.integer(forKey: Keys.AppID.rawValue) {
			
			defaults.set(current.AppID, forKey: Keys.AppID.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "AppID Updated -> \(current.AppID)")
		}
		
		if current.HandsetUDID != defaults.string(forKey: Keys.HandsetUDID.rawValue) {
			defaults.set(current.HandsetUDID, forKey: Keys.HandsetUDID.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "HandsetUDID Updated -> \(current.HandsetUDID)")
		}
		
		if current.AdvertisingID != defaults.string(forKey: Keys.AdvertisingID.rawValue) {
			defaults.set(current.AdvertisingID, forKey: Keys.AdvertisingID.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "AdvertisingID Updated -> \(current.AdvertisingID)")
		}
		
		if current.HasLimitedAd != defaults.bool(forKey: Keys.HasLimitedAd.rawValue) {
			defaults.set(current.HasLimitedAd, forKey: Keys.HasLimitedAd.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "HasLimitedAd Updated -> \(current.HasLimitedAd)")
		}
		
		if current.HasPushEnabled != defaults.bool(forKey: Keys.HasPushEnabled.rawValue) {
			defaults.set(current.HasPushEnabled, forKey: Keys.HasPushEnabled.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "HasPushEnabled Updated -> \(current.HasPushEnabled)")
		}
		
		if current.HasLocationEnabled != defaults.bool(forKey: Keys.HasLocationEnabled.rawValue) {
			defaults.set(current.HasLocationEnabled, forKey: Keys.HasLocationEnabled.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "HasLocationEnabled Updated -> \(current.HasLocationEnabled)")
		}
		
		if current.DeviceModel != defaults.string(forKey: Keys.DeviceModel.rawValue) {
			defaults.set(current.DeviceModel, forKey: Keys.DeviceModel.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "DeviceModel Updated -> \(current.DeviceModel)")
		}
		
		if current.DeviceName != defaults.string(forKey: Keys.DeviceName.rawValue) {
			defaults.set(current.DeviceName, forKey: Keys.DeviceName.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "DeviceName Updated -> \(current.DeviceName)")
		}
		
		if current.SystemVersion != defaults.string(forKey: Keys.SystemVersion.rawValue) {
			defaults.set(current.SystemVersion, forKey: Keys.SystemVersion.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "SystemVersion Updated -> \(current.SystemVersion)")
		}
		
		if current.AppVersion != defaults.string(forKey: Keys.AppVersion.rawValue) {
			defaults.set(current.AppVersion, forKey: Keys.AppVersion.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "AppVersion Updated -> \(current.AppVersion)")
		}
		
		if current.DeviceToken != defaults.string(forKey: Keys.DeviceToken.rawValue) {
			defaults.set(current.DeviceToken, forKey: Keys.DeviceToken.rawValue)
			current.hasChanges = true
			
			Log.add(detailedLog: "DeviceToken Updated -> \(current.DeviceToken)")
		}
		
		//    if current.GeowaveBeacons != defaults.data(forKey: Keys.GeowaveBeacons.rawValue)  {
		//      defaults.set(current.GeowaveBeacons, forKey: Keys.GeowaveBeacons.rawValue)
		//      current.hasChanges = true
		//    }
		
		if current.LastRegisterDevice != defaults.object(forKey: Keys.LastRegisterDevice.rawValue) as? Date {
			defaults.set(current.LastRegisterDevice, forKey: Keys.LastRegisterDevice.rawValue)
		}
		
		if current.LastBeaconEnterRegion != defaults.object(forKey: Keys.LastBeaconEnterRegion.rawValue) as? Date {
			defaults.set(current.LastBeaconEnterRegion, forKey: Keys.LastBeaconEnterRegion.rawValue)
		}
		
		if current.LastBeaconRefresh != defaults.object(forKey: Keys.LastBeaconRefresh.rawValue) as? Date {
			defaults.set(current.LastBeaconRefresh, forKey: Keys.LastBeaconRefresh.rawValue)
		}
		
		if current.CurrentCity != defaults.string(forKey: Keys.CurrentCity.rawValue) {
			defaults.set(current.CurrentCity, forKey: Keys.CurrentCity.rawValue)
			
			Log.add(detailedLog: "Current City Updated -> \(current.CurrentCity)")
		}
		
		defaults.set(current.hasChanges, forKey: Keys.HasChanges.rawValue)
	}
	
	class func updateChanges() {
		
		if current.hasChanges {
			
			GeowaveService.RegisterDevice()
			GeowaveService.TrackPushSettings()
			//GeowaveService.TrackLocationSettings()
			
			current.hasChanges = false
			save()
		}
	}
	
	// MARK: SET VALUES
	
	class func set(AppID: Int) {
		current.AppID = AppID
		save()
	}
	
	class func set(SDKVersion: String) {
		current.SDKVersion = SDKVersion
		save()
	}
	
	class func set(HasPushEnabled: Bool) {
		current.HasPushEnabled = HasPushEnabled
		save()
	}
	
	class func set(HasLocationEnabled: Bool) {
		current.HasLocationEnabled = HasLocationEnabled
		save()
	}
	
	class func set(DeviceToken: String) {
		current.DeviceToken = DeviceToken
		save()
	}
	
	class func set(DeviceName: String) {
		current.DeviceName = DeviceName
		save()
	}
	
	class func set(LastRegisterDevice: Date) {
		current.LastRegisterDevice = LastRegisterDevice
		save()
	}
	
	class func set(LastBeaconRefresh: Date) {
		current.LastBeaconRefresh = LastBeaconRefresh
		save()
	}
	
	//  class func set(GeowaveBeacons: Data) {
	//    current.GeowaveBeacons = GeowaveBeacons
	//    save()
	//  }
	
	class func set(LastBeaconEnterRegion: Date) {
		current.LastBeaconEnterRegion = LastBeaconEnterRegion
		save()
	}
	
	class func set(CurrentCity: String) {
		current.CurrentCity = CurrentCity
		save()
	}
	
	// MARK: HELPERS
	
	class func valid() -> Bool {
		
		var isValid = true
		
		if(current.AppID == 0) {
			isValid = false
			Log.add(warning: "Invalid AppID -> \(current.AppID)")
		}
		return isValid
	}
	
	class func registrationIsOutdated() -> Bool {
		return -current.LastRegisterDevice.timeIntervalSinceNow > current.MAX_TIME_BETWEEN_REGISTRATION
	}
	
	class func beaconsListIsOutdated() -> Bool {
		return -current.LastBeaconRefresh.timeIntervalSinceNow > current.MAX_TIME_BETWEEN_BEACON_REFRESH
	}
	
	class func beaconsAlertIsOutdated() -> Bool {
		return -current.LastBeaconEnterRegion.timeIntervalSinceNow > current.MAX_TIME_BETWEEN_BEACON_ALERT
	}
	
	class func fullRegistration() {
		
		GeowaveService.RegisterDevice()
		GeowaveService.TrackPushSettings()
		//GeowaveService.TrackLocationSettings()
		
		UserSettings.set(LastRegisterDevice: Date())
	}
	
	class func getNetworkState() -> Reachability.NetworkStatus {
		return current.reachability.currentReachabilityStatus
	}
	
	func getAppID() -> Int {
		if  let infoPlist = Bundle.main.infoDictionary {
			if let appID = infoPlist["GeowaveAppID"] as? Int {
				return appID
			}
		}
		return 0
	}
	
}
