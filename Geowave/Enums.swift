//
//  Enums.swift
//  geowave
//
//  Created by Doug Chisholm on 28/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation

enum Keys : String {
	
	case AppID = "GW_AppID"
	case AdvertisingID = "GW_AdvertisingID"
	case HandsetUDID = "GW_HandsetUDID"
	case HasLimitedAd = "GW_HasLimitedAd"
	case HasPushEnabled = "GW_HasPushEnabled"
	case HasLocationEnabled = "GW_HasLocationEnabled"
	case DeviceModel = "GW_DeviceModel"
	case SystemVersion = "GW_SystemVersion"
	case SDKVersion = "GW_SDKVersion"
	case AppVersion = "GW_AppVersion"
	case DeviceToken = "GW_DeviceToken"
	case DeviceName = "GW_DeviceName"
	case HasChanges = "GW_HasChanges"
	
	case LastRegisterDevice = "GW_LastRegisterDevice"
	case LastBeaconRefresh = "GW_LastBeaconRefresh"
	case LastBeaconEnterRegion = "GW_LastBeaconEnterRegion"
	case CurrentCity = "GW_CurrentCity"
	
	case GeowaveBeacons = "GW_Beacons"
}

enum API : String {
	
	static let BaseURL_BitQueen = "https://prod-bitqueen.azurewebsites.net/"
	static let baseURL = "https://stag-freshstamp.azurewebsites.net/"
	static let baseURL_Transactions = "https://prod-23.northeurope.logic.azure.com/"
	static let baseURL_DataReq = "https://prod-44.northeurope.logic.azure.com:443/"
	case RegisterDevice = "api/register"
	case PushSettings = "api/pushtoken"
	case PersonalDetails = "api/personaldetails"
	case LocationSettings = "api/tracklocationsettings"
	case Location = "api/ping"
	case FrequentLocation = "api/visit"
	case AppOpen = "api/open"
	case PushFeedback = "api/trackpushfeedback"
	case Segment = "api/tracksegment"
	case Funnel = "api/trackfunnel"
	case Beacon = "api/beacons"
	case LoyaltyFeedback = "api/loyalty"
	case FreshstampRequest = "api/freshstamprequest"
	case Optin = "api/optin"
	case OnboardDetails = "api/analytics"
	case AuthCode = "workflows/c4a6909c62dc4aaead1aa12bc2077bf7/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=_ebz5dLbe-7-wnV5QZ3iGI-1jFeDLZ73G0pBGl8AriU"
	case DataRequest = "workflows/ae26466436d941b3b1244821f3890d6d/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=MKKexyYxzojiEES6ZMLb3yNW9fgnp6O4IQROjYyUw24"
}

enum LocationType {
	case Always
	case WhenInUse
}
