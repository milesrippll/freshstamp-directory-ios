//
//  Log
//
//  Created by Doug Chisholm on 21/10/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation

public enum LogStyle: String, Equatable {
  case None = ""
  case Detailed = "Detailed"
  case File = "File"
  case Simple = "Simple"
}

class Log {
  
  public var Styles: [LogStyle] = []
  let dateFormatter: DateFormatter = DateFormatter.init()
  //public var fullLog: String = ""

  static let current : Log = {
    let current = Log()
    
    current.dateFormatter.timeStyle = .medium
    current.dateFormatter.dateStyle = .short
    
    current.Styles = [LogStyle.None]
    
    return current
  }()
  
  private class func logDate() -> String {
    return current.dateFormatter.string(from: Date.init())
  }
  
  class func add(log: String) {

    if(current.Styles.contains(.File)) {
      writeToFile(log: "\(logDate()): \(log)")
    }
    
    if(current.Styles.contains(.Simple) || current.Styles.contains(.Detailed)) {
      print("GEOWAVE (\(logDate())): \(log)")
    }

    //current.fullLog += log + "\n"
  }
  
  class func add(warning: String) {

    if(current.Styles.contains(.File)) {
      writeToFile(log: "\(logDate()): \(warning)")
    }

    //current.fullLog += warning + "\n"

    print("GEOWAVE (\(logDate())): \(warning)")
  }
  
  class func add(detailedLog: String) {

    if(current.Styles.contains(.File)) {
      writeToFile(log: "\(logDate()): \(detailedLog)")
    }
    
    if(current.Styles.contains(.Detailed)) {
      print("GEOWAVE (\(logDate())): \(detailedLog)")
    }

    //current.fullLog += detailedLog + "\n"
  }
  
  class func writeToFile(log: String) {
    
    let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!)
    let logPath = documentsPath.appendingPathComponent("geowave_debug.txt")
    
    var file = FileHandle.init(forWritingAtPath: (logPath?.path)!)
    
    if(file == nil) {
      FileManager.default.createFile(atPath: (logPath?.path)!, contents: nil, attributes: nil)
      
      file = FileHandle.init(forWritingAtPath: (logPath?.path)!)
    }
    
    if(file != nil){
      // Set the data we want to write
      let data = ("\n\(log)").data(using: String.Encoding.ascii)
      
      //move to end
      file?.seekToEndOfFile()
      
      // Write it to the file
      file?.write(data!)
      
      // Close the file
      file?.closeFile()
    }
    else {
      print("GEOWAVE: Error writing log file")
    }
    
    
  }
}

  
