//
//  AppDelegate.swift
//  freshstamp
//
//  Created by Doug Chisholm on 08/12/2016.
// Updated...
//  Copyright © 2016 rippll. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications

extension UserDefaults {

    func hasValue(forKey key: String) -> Bool {
        return nil != object(forKey: key)
    }
}


extension String {
    func substring(location: Int, length: Int) -> String? {
        guard characters.count >= location + length else { return nil }
        let start = index(startIndex, offsetBy: location)
        let end = index(startIndex, offsetBy: location + length)
        return substring(with: start..<end)
    }
}

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
	
	var window: UIWindow?
	
	 func applicationDidBecomeActive(_ application: UIApplication) {
        
		// refreshes data store
		FreshStampStore.instance.refresh()
        TransactionLoyaltyCard.instance.checkLoyaltyCardSetup() { (output) in
            
        }
    }

    static func checkCashbackAmount(isOnline : Bool, completion: @escaping (String)->()) {
            
            var request = URLRequest(url: URL(string: "https://prod-21.northeurope.logic.azure.com:443/workflows/fef09c565fa44617b20cd83b449234ce/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=CFdWyNivKAqEBg6Eenl1OGoO1ylIMh_XWqTGL0qYG6k")!)
            request.httpMethod = "GET"
            let session = URLSession.init(configuration: URLSessionConfiguration.default)
            session.dataTask(with: request) {data,response,error in
                if let data = data {
                    var contents = String(data: data, encoding: .ascii)
                    
                    if (contents != nil) {
                        var CashbackAmount : String = "5"
                        //CashbackAmount = "£" + contents!
                        CashbackAmount = contents!
                        
                        UserDefaults.standard.set(CashbackAmount, forKey: "CashbackAmount")
                    } else {
                       UserDefaults.standard.set("5", forKey: "CashbackAmount")
                    }
                }
                }.resume()
        
    }
    
    static func checkCashbackAmountReceipts(isOnline : Bool, completion: @escaping (String)->()) {
        
        var request = URLRequest(url: URL(string: "https://prod-43.northeurope.logic.azure.com:443/workflows/a405be51693c4cd98d604871d3e75757/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=5IHKtJRtOg6ahXuRuVsSI8DXmC-qMqpjrCnK9rNgwHQ")!)
        request.httpMethod = "GET"
        let session = URLSession.init(configuration: URLSessionConfiguration.default)
        session.dataTask(with: request) {data,response,error in
            if let data = data {
                var contents = String(data: data, encoding: .ascii)
            
                if (contents != nil) {
                    var CashbackAmount : String = "£5"
                    CashbackAmount = "£" + contents!
                    UserDefaults.standard.set(CashbackAmount, forKey: "CashbackAmountReceipts")
                } else {
                    UserDefaults.standard.set("£5", forKey: "CashbackAmountReceipts")
                }
            }
            }.resume()
        
    }
    
	 func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
        
       checkValues()
        
        if (UserDefaults.standard.object(forKey: "CashbackAmount") == nil) {
                UserDefaults.standard.set("", forKey: "CashbackAmount")
        }
        
        if (UserDefaults.standard.object(forKey: "dataDOB") == nil) {
                          UserDefaults.standard.set("", forKey: "dataDOB")
        }
        
        if (UserDefaults.standard.object(forKey: "dataDOB") == nil) {
                          UserDefaults.standard.set("", forKey: "dataDOB")
        }
        
        if (UserDefaults.standard.object(forKey: "dataIntroMobile") == nil) {
                   UserDefaults.standard.set("", forKey: "dataIntroMobile")
        }
        
        if (UserDefaults.standard.object(forKey: "merchantChosenForCashbackCard") == nil) {
            UserDefaults.standard.set("", forKey: "merchantChosenForCashbackCard")
        }
        
        if (UserDefaults.standard.object(forKey: "dataMobile") == nil) {
            UserDefaults.standard.set("", forKey: "dataMobile")
        }
        
        if (UserDefaults.standard.object(forKey: "CashbackAmountReceipts") == nil) {
                   UserDefaults.standard.set("£5", forKey: "CashbackAmountReceipts")
        }
        
		// starts geowave services
		Geowave.start()
		
		return true
	}
    
    func checkValues() {
        AppDelegate.checkCashbackAmount(isOnline: true) { (output) in
            
        }
        
        AppDelegate.checkCashbackAmountReceipts(isOnline: true) { (output) in
            
        }
        
        //
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        checkValues()
        
       // self.NextViewController(storybordid: "TransactionLoyaltyCard")
        
        TransactionLoyaltyCard.instance.checkLoyaltyCardSetup() { (output) in
        }
        
        Businesses.instance.checkLoyaltyCardStamps() { (output) in
        }
    }
	

    func NextViewController(storybordid:String)
    {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let exampleVC = storyBoard.instantiateViewController(withIdentifier:storybordid )
        // self.present(exampleVC, animated: true)
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = exampleVC
        self.window?.makeKeyAndVisible()
    }

	
	func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
		let queryItems = url.queryItems
		if let code = queryItems["code"] {
		
			GeowaveService.AuthCode(code: code)
			self.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SuccessLogin")
		///}
//		func checkLogin(){
//			if code.isEmpty {
//				self.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CashRewardsFailed")
//			} else {
//				//self.window?.rootViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SuccessLogin")
//			}
//		}
		}
		return true
	}
	
	
	}


extension URL {
	public var queryItems: [String: String] {
		var params = [String: String]()
		return URLComponents(url: self, resolvingAgainstBaseURL: false)?
			.queryItems?
			.reduce([:], { (_, item) -> [String: String] in
				params[item.name] = item.value
				return params
			}) ?? [:]
	}
}
