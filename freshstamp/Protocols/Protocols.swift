//
//  FreshStampProtocols.swift
//  freshstamp
//
//  Created by Tomasz Walis-Walisiak on 22/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation

protocol StoreProtocol {
  var storeKey: String { get }
  associatedtype ValueType
  func set(value: ValueType, for key: String)
  func getValue(for key: String) -> ValueType
  init(_ storeKey: String)
}

protocol ScanProtocol {
  func CodeScanned(code: String) -> Bool
  func Dismissed()
}
