//
//  LoyaltyViewController.swift
//  freshstamp
//
//  Created by Doug Chisholm on 08/12/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import UIKit
import CoreLocation
import SafariServices
import AVFoundation

class LoyaltyViewController: UIViewController, ScanProtocol, SFSafariViewControllerDelegate {

  func Dismissed() {
    self.reload() 
  }

  var url = ""

  func showWeb() {

    if let url = URL(string: self.url) {
      let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
      vc.delegate = self
      present(vc, animated: true)
    }
  }

  func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
    dismiss(animated: true)
  }

  @IBAction func secondaryActionPressed(_ sender: Any) {

    if url != "" {
      self.showWeb()
    }
  }
  @IBOutlet weak var secondaryActionBtn: UIButton!

  //MARK: IBOutlets
  @IBOutlet weak var backgroundImage: UIImageView!
  @IBOutlet weak var cardImgView: UIImageView!
  @IBOutlet weak var cardFrame: UIView!
  @IBOutlet weak var actionBtn: UIButton!
  @IBOutlet weak var termsBtn: UIButton!
  @IBOutlet weak var offerLabel: UILabel!

  fileprivate let max_countdown: Double = 60.0
  fileprivate var countdown: Double = 0.0
  fileprivate var timer: Timer!

  var fsCard: FreshStamp!
  var loyaltyCard: LoyaltyCard!
  var scannedCode: String = ""
  var displayScanner: Bool = false


  func CodeScanned(code: String) -> Bool{
    return loyaltyCard.checkin(code: code)
  }

  //MARK: IBActions
  @IBAction func actionBtnPressed(_ sender: AnyObject) {

    switch(currentAction) {

    case .redeem:
      self.present(redemptionAlert, animated: true, completion: nil)
      refreshStampsView()

    case .cooldown:
      self.present(cooldownAlert, animated: true, completion: nil)

    case .scan:
      scannerVC.modalPresentationStyle = .overCurrentContext
      self.present(scannerVC, animated: true, completion: nil)

      //
      //      self.performSegue(withIdentifier: "showScanner", sender: self)
    //
    case .authorization:
      self.present(authorizationAlert, animated: true, completion: nil)
    }
  }
    
    func hasCameraPermission() -> Bool {
        
        let cameraMediaType = AVMediaType.video
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)
        
        switch cameraAuthorizationStatus {
        case .denied: return false
        case .authorized: return true
        case .restricted: return false
            
        case .notDetermined:
            return false
        }
    }

  fileprivate var currentAction: ButtonAction {

    if ![CLAuthorizationStatus.authorizedAlways].contains(CLLocationManager.authorizationStatus()) || !hasCameraPermission() {
      return .authorization
    }

    if loyaltyCard.canRedeem {
      return .redeem
    }

    if loyaltyCard.canCheckIn {
      return .scan
    }

    return .cooldown
  }

  fileprivate lazy var redemptionAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.confirmation.description,
      message: Alert.redemption.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.redeem.description,
        style: .default,
        handler: {
          action in self.displayCountdown()
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )
    return alert
  }()

  fileprivate lazy var cooldownAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.retry.description,
      message: Alert.cooldown.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.ok.description, style: UIAlertActionStyle.cancel, handler: nil))

    return alert
  }()

  fileprivate lazy var authorizationAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.disabled.description,
      message: Alert.authorization.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.settings.description,
        style: .default,
        handler: {
          action in UIApplication.shared.openURL(URL.init(string: UIApplicationOpenSettingsURLString)!)
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )

    return alert
  }()

  fileprivate lazy var countdownAlert: UIAlertController = {

    let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
    alert.message = self.fsCard.card.redemptionCode

    let frame = CGRect(
      x: self.redemptionAlert.view.bounds.origin.x,
      y: self.redemptionAlert.view.bounds.origin.y,
      width: self.redemptionAlert.view.frame.width,
      height: self.redemptionAlert.view.frame.height + self.loadingIndicator.frame.height
    )

    alert.view.frame = frame

    alert.view.addSubview(self.loadingIndicator)

    return alert
  }()

  var loadingIndicator: UIActivityIndicatorView {

    let indicator = UIActivityIndicatorView(frame: redemptionAlert.view.bounds)

    indicator.center =  CGPoint(x: redemptionAlert.view.frame.width / 2.0, y: redemptionAlert.view.frame.height - indicator.frame.height)
    indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    indicator.hidesWhenStopped = true
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray

    return indicator
  }

  lazy var scannerVC: ScannerViewController = {

    let storyboard = UIStoryboard.init(name: "Scanner", bundle: nil)

    let scannerVC: ScannerViewController = storyboard.instantiateInitialViewController() as! ScannerViewController

    scannerVC.delegate = self

    return scannerVC
  }()

  override func viewDidLoad() {
    super.viewDidLoad()



    self.navigationController?.toolbar.alpha = 0.0

    self.reload()

    self.view.backgroundColor = UIColor.white

//    self.navigationItem.title = fsCard.business.name
		
		
    self.navigationItem.backBarButtonItem?.tintColor = .white

    offerLabel.textColor = UIColor(hexString: fsCard.card.textColor)
    offerLabel.text = fsCard.card.offer
    offerLabel.adjustsFontSizeToFitWidth = true

    self.view.backgroundColor = UIColor(hexString: fsCard.card.backgroundColor)
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    if segue.identifier == "showScanner" {

      if let destinationVC = segue.destination as? ScannerViewController {
        destinationVC.cardCode = "qrcode"
        destinationVC.card = loyaltyCard
        destinationVC.delegate = self
      }
    }

    self.navigationController?.navigationBar.tintColor = UIColor.white
  }

  func reload() {

    if loyaltyCard == nil {
      loadCard()
    } else {
      refreshStampsView()
    }

    refreshActionButton()
  }

  override func viewDidLayoutSubviews() {

    refreshStampsView()
  }

  public func toggleScanner() {
    self.displayScanner = !displayScanner
  }

  override func viewWillAppear(_ animated: Bool) {

    if UserDefaults.standard.bool(forKey: "displayScanner") {
      self.toggleScanner()
      UserDefaults.standard.set(false, forKey: "displayScanner")
      UserDefaults.standard.synchronize()
    }
  }

  override func viewDidAppear(_ animated: Bool) {

    if self.displayScanner {
      self.toggleScanner()
      self.performSegue(withIdentifier: "showScanner", sender: self)
    } else {
      self.reload()
    }

  }
}

extension LoyaltyViewController {

  func checkScannedQRCode(code: String) -> Bool {
    return loyaltyCard.checkin(code: code)
  }

  func loadCard() {

    loyaltyCard = LoyaltyCard(data: self.fsCard)

  }

  func refreshStampsView() {

    var noOfRows = 0

    if self.loyaltyCard.stamps <= 3 { noOfRows = 1 }

    else if self.loyaltyCard.stamps <= 10 { noOfRows =  2 }

    else { noOfRows = 3 }

    var rowStamps: [Int] = [Int]()
    var rowCheckins: [Int] = [Int]()

    for _ in 0..<noOfRows {
      rowStamps.append(0)
      rowCheckins.append(0)
    }

    var rowIndex = 0

    for _ in 0..<loyaltyCard.stamps {
      rowStamps[rowIndex] += 1
      rowIndex += 1
      rowIndex = rowIndex == noOfRows ? 0 : rowIndex //resets row
    }

    var stampsChecked = 0
    for i in 0..<noOfRows {

      for _ in 0..<rowStamps[i] {
        if stampsChecked < loyaltyCard.checkins {
          rowCheckins[i] += 1
          stampsChecked += 1
        }
      }
    }

    //stamp width: size & inset
    let loyalty_card_width = cardFrame.frame.width       // loyalty card image stretches across screen
    let stamp_frame_width = loyalty_card_width  // use 90% to avoid drawing over card borders - can be ajusted
    let stamp_frame_width_inset = (loyalty_card_width - stamp_frame_width) / CGFloat(2.0)   // calculate inset

    //stamp height: size & inset
    let loyalty_card_height = cardFrame.frame.height
    let stamp_frame_height = (loyalty_card_height) / CGFloat(noOfRows) // use 80% to avoid drawing over card borders - can be ajusted
    let stamp_frame_height_inset = (loyalty_card_height - (stamp_frame_height * CGFloat(noOfRows))) / CGFloat(2.0)  // calculate inset

    //max stamp size & margins
    // x
    let max_margin_x = 0.005 * stamp_frame_width // 0.5% of the total frame width
    // (total frame width - (all margins)) / total stamps in first row (row with the most possible stamps)
    let max_stamp_x = (stamp_frame_width - ( (CGFloat(rowStamps[0] - 1) * max_margin_x ))) / CGFloat(rowStamps[0])


    // y
    let max_margin_y = 0.02 * stamp_frame_height // 2% of the total frame height
    let max_stamp_y = stamp_frame_height - (2 * max_margin_y) // total frame size - (all row margins) / total stamps in first row


    // stamp size and margin
    let stamp_max_size = max_stamp_x > max_stamp_y ? max_stamp_y : max_stamp_x
    let stamp_margin_x = (stamp_frame_width  - ( CGFloat(rowStamps[0]) * stamp_max_size )) / CGFloat(rowStamps[0]-1)
    let stamp_margin_y = (stamp_frame_height - stamp_max_size) / 2.0

    var delay: Double = 0.1

    for i in 0..<noOfRows {

      // stamp row frame
      let stamp_frame = UIView.init(frame: CGRect.init(origin: CGPoint.init(x: stamp_frame_width_inset, y: stamp_frame_height_inset + ( CGFloat(i) * stamp_frame_height )), size: CGSize.init(width: stamp_frame_width, height: stamp_frame_height)))

      // row inset based on number of stamps to center in frame
      let stamp_inset_x = ( stamp_frame_width - ( ( CGFloat(rowStamps[i]) * stamp_max_size )  + ( CGFloat(rowStamps[i]-1) * stamp_margin_x ) ) ) / 2.0

      for x in 0..<rowStamps[i] {

        // calculate stamp position
        var stamp_origin = CGPoint.zero

        stamp_origin.x += stamp_inset_x + (CGFloat(x) * stamp_max_size) + (CGFloat(x) * stamp_margin_x)

        stamp_origin.y += stamp_margin_y

        // stamp image
        let img = UIImageView.init(frame: CGRect.init(origin: stamp_origin, size: CGSize.init(width: stamp_max_size, height: stamp_max_size)))

        img.alpha = 0

        if x < rowCheckins[i] {
            switch(self.fsCard.card.stampTheme)
            {
            case "coffee":
                img.image = #imageLiteral(resourceName: "coffee_stamp_checked")
                break
            case "heart":
                img.image = #imageLiteral(resourceName: "heart_stamp_checked")
                break
            case "barber":
                img.image = #imageLiteral(resourceName: "barber_stamp_checked")
                break
            default:
                img.image = #imageLiteral(resourceName: "default_stamp_checked")
            }
        } else {
            switch(self.fsCard.card.stampTheme)
            {
            case "coffee":
                img.image = #imageLiteral(resourceName: "coffee_stamp_unchecked")
                break
            case "heart":
                img.image = #imageLiteral(resourceName: "heart_stamp_unchecked")
                break
            case "barber":
                img.image = #imageLiteral(resourceName: "barber_stamp_unchecked")
                break
            default:
                img.image = #imageLiteral(resourceName: "default_stamp_unchecked")
            }
        }

        stamp_frame.addSubview(img)

        UIView.animate(withDuration: 0.7, delay: delay, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: .curveEaseIn, animations: {
          img.alpha = 1.0
        }, completion: nil)
        delay += 0.1
      }

      cardFrame.addSubview(stamp_frame)
    }
  }


  func refreshActionButton() {

    let buttonColor = UIColor.init(hexString: fsCard.card.butonColor)
    actionBtn.setTitle(loyaltyCard.canRedeem ? ButtonAction.redeem.title : ButtonAction.scan.title, for: UIControlState.normal )
    actionBtn.layer.borderColor = UIColor.black.cgColor
    actionBtn.backgroundColor = UIColor.clear
    actionBtn.setBackgroundImage(UIImage.image(with: buttonColor), for: UIControlState.normal)
    actionBtn.tintColor = UIColor(hexString: fsCard.card.butonTextColor)
    actionBtn.layer.cornerRadius = 5.0;
    actionBtn.layer.borderWidth = 1.0
    actionBtn.clipsToBounds = true

    self.url = fsCard.business.yelpUrl

    if url == "" {
      secondaryActionBtn.alpha = 0.0
    } else {
      secondaryActionBtn.alpha = 1.0
    }

    secondaryActionBtn.layer.borderColor = UIColor.white.cgColor
    secondaryActionBtn.backgroundColor = UIColor.clear
    secondaryActionBtn.setBackgroundImage(UIImage.image(with: .red), for: UIControlState.normal)
    secondaryActionBtn.tintColor = .white
    secondaryActionBtn.layer.cornerRadius = 5.0;
    secondaryActionBtn.layer.borderWidth = 1.0
    secondaryActionBtn.clipsToBounds = true
  }

  func refreshStampRow(_ stack: UIStackView, stampsPerRow: Int, checkins: Int) {

    var checkedStamps = 0
    var delay: Double = 0.1

    for stamp in stack.arrangedSubviews {

      let img = stamp as! UIImageView

      img.alpha = 0

      if checkedStamps < stampsPerRow {
        stamp.isHidden = false

        //set image
        if checkedStamps < checkins {
            switch(self.fsCard.card.stampTheme)
            {
            case "coffee":
                img.image = #imageLiteral(resourceName: "coffee_stamp_checked")
                break
            case "heart":
                img.image = #imageLiteral(resourceName: "heart_stamp_checked")
                break
            case "barber":
                img.image = #imageLiteral(resourceName: "barber_stamp_checked")
                break
            default:
                img.image = #imageLiteral(resourceName: "default_stamp_checked")
            }
        } else {
            switch(self.fsCard.card.stampTheme)
            {
            case "coffee":
                img.image = #imageLiteral(resourceName: "coffee_stamp_unchecked")
                break
            case "heart":
                img.image = #imageLiteral(resourceName: "heart_stamp_unchecked")
                break
            case "barber":
                img.image = #imageLiteral(resourceName: "barber_stamp_unchecked")
                break
            default:
                img.image = #imageLiteral(resourceName: "default_stamp_unchecked")
            }
        }

      } else {
        stamp.isHidden = true
      }

      //animate
      UIView.animate(withDuration: 0.7, delay: delay, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: .curveEaseIn, animations: {
        img.alpha = 1.0
      }, completion: nil)
      delay += 0.1
      checkedStamps   += 1
    }
  }

  func displayCountdown() {

    loyaltyCard.redeem()

    countdown = 60.0

    self.present(countdownAlert, animated: true, completion: nil)

    timer = Timer(
      timeInterval: 1.0,
      target: self,
      selector: #selector(LoyaltyViewController.refreshCountdown),
      userInfo: nil,
      repeats: true
    )

    RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)

    Utils.delay(max_countdown, closure: {

      self.countdownAlert.dismiss(
        animated: true,
        completion: {
          self.timer.invalidate()
          
          self.reload()

          self.refreshStampsView()
      }
      )
    })

    timer.fire()
    refreshCountdown()

    loadingIndicator.startAnimating()
  }

  @objc func refreshCountdown() {

    countdownAlert.title = "\(Alert.countdown.description) \(Int(countdown)) seconds"

    if(countdown > 0){
      countdown -= 1
    }
  }
}


