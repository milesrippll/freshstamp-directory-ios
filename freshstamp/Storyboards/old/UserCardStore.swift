//
//  UserCardStore.swift
//  freshstamp
//
//  Created by Doug Chisholm on 04/10/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation

class UserCardStore: StoreProtocol {

  let storeKey: String

  func set(value: [FreshStamp]?, for key: String) {
    let defaults = UserDefaults.standard
    try? defaults.setValue(PropertyListEncoder().encode(value), forKey: "\(storeKey)\(key)")
  }

  func getValue(for key: String) -> [FreshStamp]? {
    let defaults = UserDefaults.standard
    if let data = defaults.object( forKey: "\(storeKey)\(key)") as? Data {

      return try? PropertyListDecoder().decode(Array<FreshStamp>.self, from: data)
    }
    return nil
  }

  required init(_ storeKey: String) {
    self.storeKey = storeKey
  }

  typealias ValueType = [FreshStamp]?
}

