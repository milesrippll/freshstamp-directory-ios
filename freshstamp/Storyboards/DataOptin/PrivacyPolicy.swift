//
//  PrivacyPolicy.swift
//  freshstamp
//
//  Created by Rippll Developer on 06/03/2018.
//  Copyright © 2018 rippll. All rights reserved.
//

import UIKit
import SafariServices

class PrivacyOptinViewController: UIViewController, SFSafariViewControllerDelegate, UIWebViewDelegate {
	
	@IBOutlet weak var webView: UIWebView!
	@IBOutlet weak var policyUrl: UIButton!
	@IBOutlet weak var optin: UIButton!
	@IBOutlet weak var optout: UIButton!
	@IBOutlet weak var webViewHeight: NSLayoutConstraint!
	@IBAction func webPrivacy(_ sender: Any) {
		
		if let url = URL(string: "https://www.rippll.com/privacy") {
			let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
			vc.delegate = self
			present(vc, animated: true)
		}
	}
	
//	let dataRequest = UIAlertAction(title: "Data request", style: .default) { [unowned ac] _ in
//		
//		self.dataRequest()
//		
//	}
	
	@IBAction func optedOut(_ sender: Any) {
		GeowaveService.Optin(allow: false)
		GeowaveService.TrackOnboardDetails(optedInAt: 3)
		self.dismiss(animated: true, completion: nil)
	}
	
	@IBAction func optedIn(_ sender: Any) {
		GeowaveService.Optin(allow: true)
		GeowaveService.TrackOnboardDetails(optedInAt: 2)
		self.dismiss(animated: true, completion: nil)
	}
	
	
	
//	 @IBAction func dataRequest() {
//		let message = "Enter your email below and we will send you all the data from this app that we have on our servers"
//		let ac = UIAlertController(title: "Data Request", message: message, preferredStyle: .alert)
//		let dr = UIAlertAction(title: "Send me my data", style: .default) { [unowned ac] _ in
//			let answer = ac.textFields![0]
//			let i = 0
//			GeowaveService.DataRequest(email: answer.text!)
//		}}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationItem.title = "Privacy & Data Usage"
		self.navigationController?.navigationBar.tintColor = .black
	}
	
	override func viewDidLoad() {
		policyUrl.tintColor = .black
		optout.tintColor = .red
		webView.delegate = self
		webView.loadRequest(URLRequest.init(url: URL.init(string: "https://www.rippll.com/moreinfo")!))
		webView.scrollView.isScrollEnabled = false
		webView.scrollView.bounces = false
	}
	
	func webViewDidFinishLoad(_ webView: UIWebView) {
		let height = self.webView.stringByEvaluatingJavaScript(from: "document.body.scrollHeight")
		webViewHeight.constant = CGFloat(Int(height!)!)
	}
}

