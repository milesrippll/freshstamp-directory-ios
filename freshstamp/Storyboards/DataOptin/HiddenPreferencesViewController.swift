//
//  HiddenPreferencesViewController.swift
//  freshstamp
//
//  Created by Doug Chisholm on 17/07/2018.
//  Copyright © 2018 rippll. All rights reserved.
//

import UIKit

class HiddenPreferencesViewController : UIViewController {
	
	@IBAction func toggleLoyaltyTracking(_ sender: Any) {
		UserDefaults.standard.set(loyaltyTrackingSwitch.isOn, forKey: "LoyaltyTrackingDisabled")
	}
	
	@IBAction func refreshCachePressed(_ sender: Any) {
		FreshStampStore.instance.refresh()
		let keys = CardManager.instance.myCards().map { $0.id }
		ImageStore("CardListImageCache").clearCache(for: keys)
	}
	
	@IBOutlet weak var loyaltyTrackingSwitch: UISwitch!

	override func viewDidLoad() {
		super.viewDidLoad()
		loyaltyTrackingSwitch.setOn(UserDefaults.standard.bool(forKey: "LoyaltyTrackingDisabled") , animated: true)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationItem.title = "Developer Preferences"
		self.navigationController?.navigationBar.tintColor = .black
	}
}
