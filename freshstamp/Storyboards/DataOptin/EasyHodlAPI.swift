//
//  EasyHodlAPI.swift
//  CryptoSaver
//
//  Created by Simone D'Amico on 16/08/2017.
//  Copyright © 2017 Simone D'Amico. All rights reserved.
//
import Alamofire

class EasyHodlAPI {
	static let ACCESS_TOKEN = "ACCESS_TOKEN"
	static let BASE_URL = "https://rippll-geocoder.azurewebsites.net"
	
	static func exchangeCode(code: String, failure fail: ((NSError) -> ())? = nil, success succeed: (() -> ())? = nil){
		let params: Parameters = [
			"code": code
		]
		AF.request(BASE_URL + "/code", method: .post, parameters: params).responseJSON { response in
			
			if let json = response.result.value as? [String: String]{
				let defaults = UserDefaults.standard
				defaults.set(json[ACCESS_TOKEN], forKey: ACCESS_TOKEN)
				if let s = succeed {
					s()
				}
			}
		}
	}
    
    static func getSecret() -> String? {
        let defaults = UserDefaults.standard
        return defaults.string(forKey: EasyHodlAPI.ACCESS_TOKEN)
    }
    
//    static func getTransactions(failure fail: ((NSError) -> ())? = nil, success succeed: (([Transaction]) -> ())? = nil){
//        AF.request(BASE_URL + "/transactions", headers: headers()).responseJSON { response in
//            var transactions: [Transaction] = []
//            if let transactionsJSON = response.result.value as? [Any]{
//                for (transactionJSON) in transactionsJSON {
//                    let transactionDict = transactionJSON as! [String: Any]
//                    transactions.append(Transaction(dict: transactionDict))
//                }
//            }
//            succeed!(transactions)
//        }
//    }
//
//    static func getOrders(failure fail: ((NSError) -> ())? = nil, success succeed: (([Order]) -> ())? = nil){
//        AF.request(BASE_URL + "/orders", headers: headers()).responseJSON { response in
//            var orders: [Order] = []
//            if let ordersJSON = response.result.value as? [Any]{
//                for (orderJSON) in ordersJSON {
//                    let orderDict = orderJSON as! [String: Any]
//                    orders.append(Order(dict: orderDict))
//                }
//            }
//            succeed!(orders)
//        }
//    }
	
    static func getStats(failure fail: ((NSError) -> ())? = nil, success succeed: (([String: Any]) -> ())? = nil){
        AF.request(BASE_URL + "/stats", headers: headers()).responseJSON { response in
            if let stats = response.result.value as? [String: Any]{
                succeed!(stats)
            }
        }
    }
    
    static func setKrakenKeys(key: String, secret: String, failure fail: ((NSError) -> ())? = nil, success succeed: (() -> ())? = nil) {
        let params = [
            "kraken_secret": secret,
            "kraken_key": key
        ]
        AF.request(BASE_URL + "/kraken-keys", method: .patch, parameters: params, headers: headers()).response() { _ in
            succeed!()
        }
    }
    
    private static func headers() -> HTTPHeaders{
        let headers: HTTPHeaders = [
            "Authorization": getSecret()!
        ]
        return headers
    }
    
    static func logout(){
        let defaults = UserDefaults.standard
        defaults.set(nil, forKey: ACCESS_TOKEN)
    }
}
