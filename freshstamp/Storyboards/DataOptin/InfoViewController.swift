//
//  InfoViewController.swift
//  freshstamp
//
//  Created by Doug Chisholm on 16/07/2018.
//  Copyright © 2018 rippll. All rights reserved.
//

import UIKit
import SafariServices
import MessageUI

class InfoViewController: UITableViewController, SFSafariViewControllerDelegate, MFMailComposeViewControllerDelegate {
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return 4
	}
	
	func showPrivacy() {
		let vc = UIStoryboard(name: "PrivacyOptin", bundle: nil).instantiateViewController(withIdentifier: "PrivacyOptin")
		self.navigationController?.present(vc, animated: true)
	}
	
//	func showCashRewards() {
//		let vc = UIStoryboard(name: "PrivacyOptin", bundle: nil).instantiateViewController(withIdentifier: "CashRewards")
//		self.navigationController?.present(vc, animated: true)
//	}
	
	func showDeveloperPreferences() {
		let vc = UIStoryboard(name: "PrivacyOptin", bundle: nil).instantiateViewController(withIdentifier: "DeveloperSettings")
		self.navigationController?.pushViewController(vc, animated: true)
	}
		
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		switch (indexPath.row) {
		
		
//		case 0:
//			showCashRewards()
//			break
			
			
		case 0:
						if let url = URL(string: "http://requests.freshstamp.co.uk/pages/registerwallet") {
							let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
							vc.delegate = self
						present(vc, animated: true)
			}
			break
						
			
		case 1:
            
            showPrivacy()
            break
            
			
		case 2:
            if let url = URL(string: "https://www.freshstamp.co.uk/termsofuse") {
                let vct = SFSafariViewController(url: url, entersReaderIfAvailable: true)
                vct.delegate = self
                present(vct, animated: true)
                
            }
            break
            
			

					
		case 3:
					if let url = URL(string: "http://www.freshstamp.co.uk") {
						let vctc = SFSafariViewController(url: url, entersReaderIfAvailable: false)
						vctc.delegate = self
						present(vctc, animated: true)
				
			}
			break
		
		default:
			break
		}
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.becomeFirstResponder()
	}
	
	override var canBecomeFirstResponder: Bool {
		get {
			return true
		}
	}
	
	override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
		if motion == .motionShake {
			showDeveloperPreferences()
		}
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationItem.title = "Info"
		self.navigationController?.navigationBar.tintColor = .black
	}
	
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
    
    @IBAction func support(_ sender: Any) {
        
        if (MFMailComposeViewController.canSendMail()) {
            
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients(["freshstamp@rippll.com"])
            composeVC.setCcRecipients(["miles@rippll.com","doug@rippll.com"])
            composeVC.setSubject("Freshstamp Support")
            composeVC.setMessageBody("...", isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            
        }
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
	
}
