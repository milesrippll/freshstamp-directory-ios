//
//  SecondViewController.swift
//  appsplash
//
//  Created by Doug Chisholm on 17/12/2015.
//  Copyright © 2015 Doug Chisholm. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {

	@IBOutlet weak var UIImageView: UIImageView!
	var delegate: ScanProtocol?

  @IBAction func actionButtonPressed(_ sender: Any) {
    self.handleTap()
  }
  @IBOutlet weak var scannerView: UIView!

  var captureSession: AVCaptureSession!
  var previewLayer: AVCaptureVideoPreviewLayer!
  var cardCode: String = ""
  var card: LoyaltyCard!
	

  func setupNavBar() {

    self.navigationItem.title = "QR Scanner"
		self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]

    self.navigationController?.navigationBar.tintColor = .black
    self.navigationController?.navigationBar.isTranslucent = false
  }

  @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
    if let swipeGesture = gesture as? UISwipeGestureRecognizer {
      switch swipeGesture.direction {
      case UISwipeGestureRecognizerDirection.right:
        print("Swiped right")
      case UISwipeGestureRecognizerDirection.down:
        print("Swiped down")
        self.handleTap()
      case UISwipeGestureRecognizerDirection.left:
        print("Swiped left")
      case UISwipeGestureRecognizerDirection.up:
        print("Swiped up")

      default:
        break
      }
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    //setupNavBar()
		
		if !Geowave.isSimulator {
			captureSession = AVCaptureSession()

			let videoInput: AVCaptureDeviceInput
			let videoCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video)

			do{
				videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice!)
			} catch { return }

			if (captureSession.canAddInput(videoInput)) {
				captureSession.addInput(videoInput)
			} else {
				failed();
				return;
			}

			let metadataOutput = AVCaptureMetadataOutput()

			if (captureSession.canAddOutput(metadataOutput)) {
				captureSession.addOutput(metadataOutput)

				metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
				metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
			} else {
				failed()
				return
			}

			previewLayer = AVCaptureVideoPreviewLayer(session: captureSession);
			previewLayer.frame = scannerView.layer.bounds;
			previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill;
			scannerView.layer.addSublayer(previewLayer);

			let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
			swipeDown.direction = UISwipeGestureRecognizerDirection.down
			self.view.addGestureRecognizer(swipeDown)
			
		} else {
			// simulator scanned card test
			// foundCode("MASTER!!WTlxPZi!!MASTER")
		}
    UIImageView.image = #imageLiteral(resourceName: "target")
    scannerView.addSubview(UIImageView)
  }

  @objc func handleTap() {
    dismiss(animated: true, completion: { self.delegate!.Dismissed() })
	}

  func failed() {
    let ac = UIAlertController(
        title: "Scanning not supported",
        message: "Your device does not support scanning a code. Please use a device with a camera.",
        preferredStyle: .alert
    )

    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    present(ac, animated: true, completion: nil)
    captureSession = nil
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    if (captureSession?.isRunning == false) {
      captureSession.startRunning();
    }
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)

    UserDefaults.standard.set(false, forKey: "displayScanner")
    UserDefaults.standard.synchronize()

    if (captureSession?.isRunning == true) {
      captureSession.stopRunning();
    }
  }

  func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection:AVCaptureConnection) {
    captureSession.stopRunning()

    if let metadataObject = metadataObjects.first {
      let readableObject = metadataObject as! AVMetadataMachineReadableCodeObject;

      AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))

      foundCode(readableObject.stringValue!);
    }
  }

  func foundCode(_ code: String) {

    if delegate!.CodeScanned(code: code){
      dismiss(animated: true, completion: {self.delegate!.Dismissed()})
    } else {
      captureSession.startRunning()
    }
  }

  override var prefersStatusBarHidden : Bool {
    return false
  }

  override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
    return .portrait
  }
}
