//
//  Onboarding3.swift
//  freshstamp
//
//  Created by Doug Chisholm on 09/10/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class OnboardingFinal: UIViewController {

	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		if let gender = genderSegment {
			gender.selectedSegmentIndex = -1
		}
	}
	
  @IBAction func finishOnboarding(_ sender: Any) {
		
		let ageSet = UserDefaults.standard.bool(forKey: "dateSet")
		
		if ageSet {
			
			GeowaveService.TrackPersonalDetails()
		
    	self.dismiss(animated: true) {}
		} else {
			
			let uncompleteAlert = UIAlertController(
					title: "Incomplete Gender/Dob",
					message: "Please swipe to fill in your Gender and DoB",
					preferredStyle: .alert
				)
				
			uncompleteAlert.addAction(
				UIAlertAction(
					title: "Ok", style: UIAlertActionStyle.cancel, handler: nil))
			
			self.present(uncompleteAlert, animated: true, completion: {})
		}
  }
	
	@IBOutlet weak var genderSegment: UISegmentedControl!
	@IBOutlet weak var dobSegment: UIDatePicker!
	@IBAction func genderChanged(_ sender: Any) {
		UserDefaults.standard.set(true, forKey: "genderSet")
		updateDetails()
	}
	@IBAction func dateChanged(_ sender: Any) {
		UserDefaults.standard.set(true, forKey: "dateSet")
		updateDetails()
	}
	
	func updateDetails() {
		
		if let gender = genderSegment {
			let male = gender.selectedSegmentIndex == 0
				UserDefaults.standard.set(male, forKey: "pd_gender")
		}
		
		if let dob = dobSegment {
			let date = dob.date
			UserDefaults.standard.set(date, forKey: "pd_dob")
		}
	}
}
