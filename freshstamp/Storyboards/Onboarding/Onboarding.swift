//
//  Onboarding.swift
//  freshstamp
//
//  Created by Doug Chisholm on 09/10/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class OnboardingPageViewController : UIPageViewController {

  private(set) lazy var orderedViewControllers: [UIViewController] = {
    return [self.newStepViewController(step: 1),
            self.newStepViewController(step: 2),
            self.newStepViewController(step: 4),
					]
  }()

  private func newStepViewController(step: Int) -> UIViewController {
    return UIStoryboard(name: "Onboarding", bundle: nil).instantiateViewController(withIdentifier: "Onboarding_\(step)")
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    dataSource = self

    if let firstViewController = orderedViewControllers.first {
      setViewControllers([firstViewController],
                       direction: .forward,
                       animated: true,
                       completion: nil)
    }
  }
}

extension OnboardingPageViewController: UIPageViewControllerDataSource {

  func pageViewController(_ pageViewController: UIPageViewController,
                          viewControllerBefore viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
      return nil
    }

    let previousIndex = viewControllerIndex - 1

    guard previousIndex >= 0 else {
      return nil
    }

    guard orderedViewControllers.count > previousIndex else {
      return nil
    }

    return orderedViewControllers[previousIndex]
  }

  func pageViewController(_ pageViewController: UIPageViewController,
                          viewControllerAfter viewController: UIViewController) -> UIViewController? {
    guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
      return nil
    }

    let nextIndex = viewControllerIndex + 1
    let orderedViewControllersCount = orderedViewControllers.count

    guard orderedViewControllersCount != nextIndex else {
      return nil
    }

    guard orderedViewControllersCount > nextIndex else {
      return nil
    }

    return orderedViewControllers[nextIndex]
  }
}
