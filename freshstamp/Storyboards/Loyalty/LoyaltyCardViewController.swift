//
//  LoyaltyCardViewController.swift
//  freshstamp
//
//  Created by Doug Chisholm on 16/11/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import CoreLocation
import AVFoundation

class LoyaltyCardViewController: UITableViewController {

  // MARK: views
	@IBOutlet weak var stampView: StampView!
  @IBOutlet weak var offerText: UILabel!
  @IBOutlet weak var termsText: UILabel!
  @IBOutlet weak var actionButton: ButtonView!
  @IBOutlet weak var yelpReviewCell: UITableViewCell!
  @IBOutlet weak var businessInfoCell: UITableViewCell!

  // MARK: card details
  var fsCard: FreshStamp!
  var loyalty: LoyaltyCard!
	var imageCache = [String:UIImage]()
	var imageStore = ImageStore("CardListImageCache")
	
  // MARK: redemption
  fileprivate let max_countdown: Double = 60.0
  fileprivate var countdown: Double = 0.0
  fileprivate var timer: Timer!

  // scanner vc
  lazy var scannerVC: ScannerViewController = {

    let storyboard = UIStoryboard.init(name: "Scanner", bundle: nil)

    let scannerVC: ScannerViewController = storyboard.instantiateInitialViewController() as! ScannerViewController

    scannerVC.delegate = self

    return scannerVC
  }()

  //MARK: alerts
  fileprivate lazy var cooldownAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.retry.description,
      message: Alert.cooldown.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.ok.description, style: UIAlertActionStyle.cancel, handler: nil))

    return alert
  }()

  fileprivate lazy var authorizationAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.disabled.description,
      message: Alert.authorization.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.settings.description,
        style: .default,
        handler: {
          action in UIApplication.shared.openURL(URL.init(string: UIApplicationOpenSettingsURLString)!)
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )

    return alert
  }()

  fileprivate lazy var countdownAlert: UIAlertController = {

    let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
    alert.message = self.fsCard.card.redemptionCode

    let frame = CGRect(
      x: self.redemptionAlert.view.bounds.origin.x,
      y: self.redemptionAlert.view.bounds.origin.y,
      width: self.redemptionAlert.view.frame.width,
      height: self.redemptionAlert.view.frame.height + self.loadingIndicator.frame.height
    )

    alert.view.frame = frame

    alert.view.addSubview(self.loadingIndicator)

    return alert
  }()

  fileprivate lazy var redemptionAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.confirmation.description,
      message: Alert.redemption.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.redeem.description,
        style: .default,
        handler: {
          action in self.displayCountdown()
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )
    return alert
  }()

  //MARK: countdown
  func displayCountdown() {

    loyalty.redeem()

    countdown = 60.0

    self.present(countdownAlert, animated: true, completion: nil)

    timer = Timer(
      timeInterval: 1.0,
      target: self,
      selector: #selector(LoyaltyCardViewController.refreshCountdown),
      userInfo: nil,
      repeats: true
    )

    RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)

    Utils.delay(max_countdown, closure: {

      self.countdownAlert.dismiss(
        animated: true,
        completion: {
          self.timer.invalidate()

          self.setupActionButton()
          self.stampView.refresh()
      }
      )
    })

    timer.fire()
    refreshCountdown()

    loadingIndicator.startAnimating()
  }

  @objc func refreshCountdown() {

    countdownAlert.title = "\(Alert.countdown.description) \(Int(countdown)) seconds"

    if(countdown > 0){
      countdown -= 1
    }
  }

  var loadingIndicator: UIActivityIndicatorView {

    let indicator = UIActivityIndicatorView(frame: redemptionAlert.view.bounds)

    indicator.center =  CGPoint(x: redemptionAlert.view.frame.width / 2.0, y: redemptionAlert.view.frame.height - indicator.frame.height)
    indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    indicator.hidesWhenStopped = true
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray

    return indicator
  }

  // MARK: - ViewDidLoad
  override func viewDidLoad() {
		
    setupNavigationBar(title: fsCard.business.name)

    setupInterface()
  }

  override func viewDidAppear(_ animated: Bool) {
    self.setupActionButton()
    self.stampView.refresh()
  }
}

// MARK:- Scanner Actions

extension LoyaltyCardViewController: ScanProtocol {
  func CodeScanned(code: String) -> Bool {
		
    return self.loyalty.checkin(code: code)
  }

  func Dismissed() {
		
    self.stampView.refresh()
    self.setupActionButton()
  }
}

// MARK: - Table View Delegate
extension LoyaltyCardViewController {

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    switch (indexPath.row) {
    case 4:
			
			if self.fsCard.business.yelpUrl != "" {
      	showWeb(url: self.fsCard.business.yelpUrl)
			}
      break
    case 5:
			
			if self.fsCard.business.webUrl != "" {
      	showWeb(url: self.fsCard.business.webUrl)
			}
      break
    default:
      break
    }
  }
}

// MARK: - Loyalty Card Actions

extension LoyaltyCardViewController {

  func setupInterface() {

    setupCard()

    setupActionButton()
  }

  func setupCard() {
		
		let urlString = "https://geowaveapi.blob.core.windows.net/freshstamp/\(self.fsCard.business.logoUrl)"
		
		if let img = imageCache[urlString] {
			self.stampView.image = img
		} else {

			if let data = imageStore.getValue(for: "\(self.fsCard.id)") {
				let image = UIImage(data: data)
				self.stampView.image = image!
				self.imageCache[urlString] = image
			} else {

				if let url = URL(string: urlString) {

					let task =
						URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

							if error == nil {
								// Convert the downloaded data in to a UIImage object
								let image = UIImage(data: data!)

								self.imageStore.set(value: data, for: "\(self.fsCard.id)")

								// Store the image in to our cache
								self.imageCache[urlString] = image
								// Update the cell
								DispatchQueue.main.async { self.stampView.image = image! }
							}
						})
					task.resume()
				}
			}
		}
		
    // set card stamps view
    self.stampView.fsCard = self.fsCard
    self.stampView.loyaltyCard = self.loyalty
		
		self.navigationItem.title = self.fsCard.business.name
		
    // set offer
    self.offerText.text = self.fsCard.card.offer

    // set terms
    self.termsText.text = "Check in store for T&C's"
  }
	
	func setupActionButton() {
		
		// sets the text on the action button depending on the current card state
    var text = ""

    switch(currentAction) {
    case .authorization:
      text = "Authorization Required"
    case .cooldown:
      text = "Collect stamp"
    case .redeem:
      text = "Redeem"
    case .scan:
      text = "Collect stamp"
    }

    self.actionButton.setTitle(text)
  }

  @IBAction func actionButtonPressed(_ sender: Any) {

    switch(currentAction) {

    case .authorization:
			
      self.present(authorizationAlert, animated: true, completion: nil)
    case .cooldown:
			
      self.present(cooldownAlert, animated: true, completion: nil)
    case .redeem:
			
      self.present(redemptionAlert, animated: true, completion: nil)
    case .scan:
      
      scannerVC.modalPresentationStyle = .overCurrentContext
      self.present(scannerVC, animated: true, completion: nil)
    }
  }

  fileprivate var currentAction: ButtonAction {

    if !hasLocationPermission() || !hasCameraPermission() {
      return .authorization
    }

    if loyalty.canRedeem {
      return .redeem
    }

    if loyalty.canCheckIn {
      return .scan
    }

    return .cooldown
  }

  func hasCameraPermission() -> Bool {

    let cameraMediaType = AVMediaType.video
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)

    switch cameraAuthorizationStatus {
    case .denied: return false
    case .authorized: return true
    case .restricted: return false

    case .notDetermined:
      return false
    }
  }

  func hasLocationPermission() -> Bool {
    return [CLAuthorizationStatus.authorizedAlways].contains(CLLocationManager.authorizationStatus())
  }
}

// MARK: - Navigation Bar

extension LoyaltyCardViewController {

  func setupNavigationBar(title: String) {
//    self.navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "back_button")
//    self.navigationController?.navigationBar.backIndicatorTransitionMaskImage  = #imageLiteral(resourceName: "back_button")
    self.navigationController?.navigationBar.tintColor = .black

//    if #available(iOS 11.0, *) {
//      self.navigationController?.navigationBar.prefersLargeTitles = true
//    }
//    self.navigationItem.titleView = getImageTitleView()
//    self.navigationItem.title = title
  }

  func getImageTitleView() -> UIView {
    let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 170, height: 44))

    let titleImageView = UIImageView(image: #imageLiteral(resourceName: "freshstamp_shadow"))

    titleImageView.frame = CGRect(x: 0, y: 0, width: titleView.frame.width, height: titleView.frame.height)

    titleView.addSubview(titleImageView)

    return titleView
  }
}

// MARK: - Web Actions
extension LoyaltyCardViewController: SFSafariViewControllerDelegate {

  func showWeb(url: String) {

    if let url = URL(string: url) {
      let vc = SFSafariViewController(url: url, entersReaderIfAvailable: true)
      vc.delegate = self
      present(vc, animated: true)
    }
  }

  func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
    dismiss(animated: true)
  }
}
