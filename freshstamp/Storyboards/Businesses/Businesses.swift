    //
    //  SettingsSlide.swift
    //  WeatherBot
    //
    //  Created by Doug Chisholm on 17/02/2019.
    //  Copyright © 2019 Rippll. All rights reserved.
    //
    
    import UIKit
    import StoreKit
    import MessageUI
    import AdSupport
    import Foundation
    
    class Businesses: UIViewController, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
        
        @IBOutlet weak var headerLabel: UILabel!
        @IBOutlet weak var bodyLabel: UILabel!
        var isCard = false
        var imagePicker: UIImagePickerController!
        var stamps : Int = 0
        @IBOutlet weak var imageView1: UIImageView!
        @IBOutlet weak var imageView2: UIImageView!
        @IBOutlet weak var imageView3: UIImageView!
        @IBOutlet weak var imageView4: UIImageView!
        @IBOutlet weak var imageView5: UIImageView!
        @IBOutlet weak var imageView6: UIImageView!
        @IBOutlet weak var footerStackView: UIStackView!
        
        static let instance: Businesses = {
            
            let instance = Businesses()
            
            return instance
        }()
        
        override func viewDidLoad() {
            super.viewDidLoad()
        
            
        }
        
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true)
            
            UIApplication.shared.statusBarStyle = .default
            
            setNeedsStatusBarAppearanceUpdate()
            
            // Get main screen bounds.
            let screenSize: CGRect = UIScreen.main.bounds
            let screenWidth = screenSize.width
            let screenHeight = screenSize.height
            print("Screen width = \(screenWidth), screen height = \(screenHeight)")
            
            if(screenHeight < 600) {
                footerStackView.isHidden = true
            } else {
                footerStackView.isHidden = false
            }
            
            // set labels
            loadLabels()
        
        }
        
        func loadLabels()  {
            // CRASH
            var cashBack : String = UserDefaults.standard.object(forKey: "CashbackAmountReceipts") as! String
            
            if (cashBack != nil) {
                
            } else {
                cashBack = "£5"
            }
            
            self.checkLoyaltyCardStamps() { (output) in
                let myString = output
                
                DispatchQueue.main.async {
                    
                        self.imageView1.image = UIImage.init(named: "clover_grey")
                        self.imageView2.image = UIImage.init(named: "clover_grey")
                        self.imageView3.image = UIImage.init(named: "clover_grey")
                        self.imageView4.image = UIImage.init(named: "clover_grey")
                        self.imageView5.image = UIImage.init(named: "clover_grey")
                        self.imageView6.image = UIImage.init(named: "clover_grey")
                        
                        self.headerLabel.text = "Scan your bank card and weekly supermarket receipts to get cashback. £5 for every 6 stamps"
                    
                        if (self.stamps > 0) {
                            self.imageView1.image = UIImage.init(named: "clover_green")
                        }
                        if (self.stamps > 1) {
                            self.imageView2.image = UIImage.init(named: "clover_green")
                        }
                        if (self.stamps > 2) {
                            self.imageView3.image = UIImage.init(named: "clover_green")
                        }
                        if (self.stamps > 3) {
                            self.imageView4.image = UIImage.init(named: "clover_green")
                        }
                        if (self.stamps > 4) {
                            self.imageView5.image = UIImage.init(named: "clover_green")
                        }
                        if (self.stamps > 5) {
                            self.imageView6.image = UIImage.init(named: "clover_green")
                            
                            self.headerLabel.text = "Congratulations! You filled your card. You will get cashback in the next 24 hours then the stamps will reset."
                        }
                    
                }
            }
            
        }
        
        func takePhoto(isCard : Bool) {
            
            self.isCard = isCard
            
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
            imagePicker.sourceType = .camera
            
            present(imagePicker, animated: true, completion: nil)
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            dismiss(animated: true, completion: nil)
        }
        
        func imagePickerController(_ picker: UIImagePickerController,
                                   didFinishPickingMediaWithInfo info: [String : Any]) {
            imagePicker.dismiss(animated: true, completion: nil)
          
            let image = info[UIImagePickerControllerOriginalImage] as? UIImage
            sendImage(image: image!)
        }
        
        @IBAction func openFreshstamp() {
            let baseURL = "http://requests.freshstamp.co.uk/pages/registerwallet"
            
            let url = URL(string: baseURL);
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                
            } else {
                // Fallback on earlier versions
            }
        }
        
        func sendImageNEWTEST(image: UIImage) {
            let headers = [
              "x-ms-blob-type": "BlockBlob",
              "User-Agent": "PostmanRuntime/7.17.1",
              "Accept": "*/*",
              "Cache-Control": "no-cache",
              "Postman-Token": "caea8fcc-30d3-48c7-aec9-138ea8d61e4f,cd4d99f1-a5ea-410f-8482-1730f839d0a8",
              "Host": "bitqueensilo.blob.core.windows.net",
              "Accept-Encoding": "gzip, deflate",
              "Content-Length": "733",
              "Content-Type": "image/png",
              "Connection": "keep-alive",
              "cache-control": "no-cache"
            ]
             
            let request = NSMutableURLRequest(url: NSURL(string: "https://bitqueensilo.blob.core.windows.net/freshstamp/REPLACEWITHUUID-REPLACEWITHRANDOMID.png?st=2019-10-09T11%3A35%3A33Z&se=2065-10-10T11%3A35%3A00Z&sp=racwdl&sv=2018-03-28&sr=c&sig=ykpFtM%2FL0%2FWJhv57JSFxTm3WuKpRcg%2BXVrGOXRLLdvM%3D")! as URL,
                                                    cachePolicy: .useProtocolCachePolicy,
                                                timeoutInterval: 10.0)
            request.httpMethod = "PUT"
            request.allHTTPHeaderFields = headers
            
            // body
           // request.http
             
            let session = URLSession.shared
            let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
              if (error != nil) {
                print(error)
              } else {
                let httpResponse = response as? HTTPURLResponse
                print(httpResponse)
              }
            })
             
            dataTask.resume()
        }
        
        func sendImage(image: UIImage) {
            var myIDFA: String? = "s"
                   // Check if Advertising Tracking is Enabled
                   if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
                       // Set the IDFA
                       myIDFA = ASIdentifierManager.shared().advertisingIdentifier.uuidString
                   } else {
                       
                       if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                           myIDFA = uuid
                       } else {
                           myIDFA = "na"
                       }
                   }
                   
            
          //  myIDFA = "360CE588-D187-4BA0-A30E-B9CBC49D5E8E"
            
            if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
                
            var subject = "..."
                
                var body = "..."
                
            if (!self.isCard) {
                    subject = "Receipt"
                body = "Attached is a receipt submission for a loyalty stamp / cashback. My IDFA is " + myIDFA!
            } else {
                subject = "Bank Card"
                body = "Attached is my bank card for freshstamp rewards and cashback. My IDFA is " + myIDFA!
                }
                
                // single button for this release:
                subject = "Bank card / recipt scan"
                body = "Attached is a bank card / receipt scan for my freshstamp loyalty card. My IDFA is " + myIDFA!
            
            mail.mailComposeDelegate = self;
                mail.setToRecipients(["freshstamp@rippll.com"])
            mail.setCcRecipients(["doug@rippll.com", "miles@rippll.com"])
            mail.setSubject(subject)
                
                
                
                mail.setMessageBody(body, isHTML: false)
          
                // let imageData: NSData = UIImagePNGRepresentation(imageView.image!)! as NSData
               
            let imageData: NSData = UIImagePNGRepresentation(image)! as NSData
                
            mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
            self.present(mail, animated: true, completion: nil)
            }
        }
        
        func sendMail(imageView: UIImageView) {
            self.sendImage(image: imageView.image!)
        }
        
        func checkLoyaltyCardStamps(completion: @escaping (String)->()) {
            
            var myIDFA: String? = "s"
            // Check if Advertising Tracking is Enabled
            if ASIdentifierManager.shared().isAdvertisingTrackingEnabled {
                // Set the IDFA
                myIDFA = ASIdentifierManager.shared().advertisingIdentifier.uuidString
            } else {
                
                if let uuid = UIDevice.current.identifierForVendor?.uuidString {
                    myIDFA = uuid
                } else {
                    myIDFA = "na"
                }
            }
            
         // MAKE THESE plist driven!
          //  myIDFA = "360CE588-D187-4BA0-A30E-B9CBC49D5E8E"
            
            let url = "https://prod-10.northeurope.logic.azure.com:443/workflows/e27b056ff8dc4229b233f6576dcba5fa/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=l3YVf9AVB9QKXcxQr7q0ymBUgaRehJpzskENAty1dBk&uuid=" + myIDFA!
            
            
            let request = URLRequest(url:URL(string: url)!)  //create a url request
            var allMerchants = "2" as String
            
            let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
                
                if let data = data {
                    do {
                        print("another call")
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]{
                            
                            if let ResultSets = json["ResultSets"] as? [String:Any]{
                                
                                if let theData = ResultSets["Table1"] as? [[String: Any]] {
                                    
                                    // if picked then return
                                    let merchantChosenForCashbackCard = UserDefaults.standard.object(forKey: "merchantChosenForCashbackCard") as! String
                                    
                                    for dataPoint in theData {
                                        
                                        /*
                                        var nextMerchant = dataPoint["Merchant"] as! String
                                        nextMerchant = nextMerchant.lowercased()
                                        
                                        allMerchants = allMerchants + "," + nextMerchant
                                        
                                        if ("rippll" == nextMerchant) {
                                            
                                            if (dataPoint["Stamps"] != nil) {
                                                self.stamps = dataPoint["Stamps"] as! Int
                                                
                                            }
                                        }*/
                                        
                                        var errorMsg = dataPoint["Error"] as! String
                                        if (errorMsg.count > 3) {
                                            DispatchQueue.main.async {
                                                self.errorPopUp(msg: errorMsg)
                                            }
                                        }
                                        
                                         self.stamps = dataPoint["Stamps"] as! Int
                                    }
                                    
                                    completion("ok")
                                    
                                } else {
                                    DispatchQueue.main.sync {
                                        
                                        //    self.headerLabel.text = "Tap below to link your bank account before you can start collecting stamps"
                                        //     self.headerButton.isHidden = false
                                        // self.pickerView.isHidden = true
                                        
                                        completion("huh")
                                        
                                    }
                                }
                            }
                            
                            
                            
                        }
                        
                    } catch {
                        // print(error.localizedDescription)
                        
                    }
                    
                    // completion(allMerchants)
                    
                }
            }
            
            task.resume()
        }
        
        func errorPopUp(msg : String) {
            let alertController = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
                       
                       // Create the actions
                       let okAction = UIAlertAction(title: "Disiss", style: UIAlertActionStyle.default) {
                           UIAlertAction in
                           
                       }
                       
                       alertController.addAction(okAction)
                       
                       // Present the controller
                       self.present(alertController, animated: true, completion: nil)
        }
        
        @IBAction func cardReg(_ sender: Any) {
            
            // pop up
            // Create the alert controller
            let alertController = UIAlertController(title: "Image quality", message: "Please choose LARGE for the image quality when asked to complete the submission. If we cant read the account number we cant pay you easily so type the account number manually if it isnt clear.", preferredStyle: .alert)
            
            // Create the actions
            let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
                UIAlertAction in
                self.takePhoto(isCard: true)
            }
            
            alertController.addAction(okAction)
            
            // Present the controller
            self.present(alertController, animated: true, completion: nil)
            
        }
        
        @IBAction func receipt(_ sender: Any) {
            
            self.takePhoto(isCard: false)
            
        }
        
        func mailComposeController(_ controller: MFMailComposeViewController,
                                   didFinishWith result: MFMailComposeResult, error: Error?) {
            // Check the result or perform other tasks.
            
            // Dismiss the mail compose view controller.
            controller.dismiss(animated: true, completion: nil)
            
            let alert = UIAlertController(title: "Scan sent", message: "Stamps will be allocated within 24 hours", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        }
        
    }
