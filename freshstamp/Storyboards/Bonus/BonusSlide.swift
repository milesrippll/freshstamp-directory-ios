    //
    //  SettingsSlide.swift
    //  WeatherBot
    //
    //  Created by Doug Chisholm on 17/02/2019.
    //  Copyright © 2019 Rippll. All rights reserved.
    //
    
    import UIKit
    import StoreKit
    
    class BonusSlide: UIView {
        
        @IBOutlet weak var Button1: UIButton!
        @IBOutlet weak var Label1: UILabel!
        @IBOutlet weak var Image1: UIImageView!
        
        @IBOutlet weak var Button2: UIButton!
        @IBOutlet weak var Label2: UILabel!
        @IBOutlet weak var Image2: UIImageView!
        
        @IBOutlet weak var Button3: UIButton!
        @IBOutlet weak var Label3: UILabel!
        @IBOutlet weak var Image3: UIImageView!
        
        func configure(itemNumber : Int) {
           
            AppDelegate.checkCashbackAmount(isOnline: true) { (output) in
               self.loadLabels(itemNumber: itemNumber)
            }
            loadLabels(itemNumber: itemNumber)
        
        }
        
        func loadLabels(itemNumber : Int)  {
            
            var cashBack : String = UserDefaults.standard.object(forKey: "CashbackAmount") as! String
            if (cashBack == nil) {
                cashBack = "£5"
            }
            
            self.Button1.setTitle("Get " + cashBack + " cashback!", for: UIControlState.normal)
            
            if(itemNumber == 1) {
                 self.Label1.text = "💰 " + NSLocalizedString("£", comment: "£") + "" + cashBack + " " + NSLocalizedString("Cashback", comment: "Cashback") + "!"
                              self.Label2.text = NSLocalizedString("Link freshstamp to your bank account to automatically get loyalty stamps when you shop at your favourite stores. Link your account now and get", comment: "Link freshstamp to your bank account to automatically get loyalty stamps when you shop at your favourite stores. Link your account now and get") + " " + NSLocalizedString("£", comment: "£") + "" + cashBack + " " + NSLocalizedString("cashback today!", comment: "cashback today!")
                
                self.Button1.isHidden = true
                //   self.Image1.image = UIImage(named: "sign.jpg")
                
                self.Button2.isHidden = true
            }
            if(itemNumber == 2) {
        
                self.Label1.text = "🔒 " + NSLocalizedString("Read only access", comment: "Read only access")
                               self.Label2.text = NSLocalizedString("Linking your bank account lets us see where you shop which is data we use for marketing analytics, but we never share this data with any other companies. We only get 'read access' to see where you shop and cannot do anything else with your account.", comment: "Linking your bank account lets us see where you shop which is data we use for marketing analytics, but we never share this data with any other companies. We only get 'read access' to see where you shop and cannot do anything else with your account.")
                
                self.Button1.isHidden = true
                
                self.Button2.isHidden = true
            }
            if(itemNumber == 3) {
                self.Label1.text = "👮🏻‍♂️ " + NSLocalizedString("FCA Approved", comment: "FCA Approved")
                self.Label2.text = NSLocalizedString("Our banking integration is approved by the FCA (Financial Conduct Authority). Tap below and we will open the secure banking login page to get started, you will get the", comment: "Our banking integration is approved by the FCA (Financial Conduct Authority). Tap below and we will open the secure banking login page to get started, you will get the") + " " + NSLocalizedString("£", comment: "£") + cashBack + " " + NSLocalizedString("in your account by the end of the day.", comment: "in your account by the end of the day.")
                self.Button1.isHidden = false
                
                var button1Text = NSLocalizedString("Get", comment: "Get")
                button1Text = button1Text + " " + NSLocalizedString("£", comment: "£") + cashBack
                  button1Text = button1Text + " " + NSLocalizedString("cashback!", comment: "cashback!")
                
                Button1.setTitle(button1Text, for: .normal)
                
                self.Button2.isHidden = false
                Button2.setTitle(NSLocalizedString("More info", comment: "More info"), for: .normal)
            }
        }
        
        override init(frame: CGRect){
            super.init(frame: frame)
            
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            
        }
        
        override func awakeFromNib() {
            super.awakeFromNib()
        }
        
        func configurationTextField(textField: UITextField!){
            textField.text = "Mobile number"
        }
        
        /*
        @IBAction func openTruelayerAuthDialog() {
            let baseURL = "https://auth.truelayer.com/"
            let redirectURI = "com.rippll.freshstamp://success"
            //let redirectURI = "https://stag-budgetv2.azurewebsites.net/Callback"
             let scopes = "info%20accounts%20balance%20transactions%20direct_debits%20products%20beneficiaries%20offline_access&enable_oauth_providers=true&enable_open_banking_providers=true&enable_credentials_sharing_providers=false"
            let clientID = "rippll-ud24"
            let url = URL(string: "\(baseURL)?enable_mock=true&response_type=code&client_id=\(clientID)&redirect_uri=\(redirectURI)&scope=\(scopes)");
            ////let url = URL(string: "\(baseURL)?enable_mock=true&response_type=code&client_id=\(clientID)&redirect_uri=\(redirectURI)&scope=\(scopes)&nonce=1444452356&state=bar");
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                
            } else {
                // Fallback on earlier versions
            }
        }*/
        
        // called when link on the slider....
        @IBAction func openTruelayerAuthDialog() {
            let baseURL = "https://auth.truelayer.com/"
            let redirectURI = "com.rippll.freshstamp://success"
            //let redirectURI = "https://stag-budgetv2.azurewebsites.net/Callback"
             let scopes = "info%20accounts%20balance%20transactions%20cards%20direct_debits%20products%20beneficiaries%20offline_access&enable_oauth_providers=true&enable_open_banking_providers=true&enable_credentials_sharing_providers=false"
            let clientID = "rippll-ud24"
            let url = URL(string: "\(baseURL)?enable_mock=true&response_type=code&client_id=\(clientID)&redirect_uri=\(redirectURI)&scope=\(scopes)");
            ////let url = URL(string: "\(baseURL)?enable_mock=true&response_type=code&client_id=\(clientID)&redirect_uri=\(redirectURI)&scope=\(scopes)&nonce=1444452356&state=bar");
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                
            } else {
                // Fallback on earlier versions
            }
        }
        
        @IBAction func openEmail() {
            let baseURL = "https://freshstamp.co.uk/cashback"
            let url = URL(string: baseURL);
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                
            } else {
                // Fallback on earlier versions
            }
        }
        
    }
