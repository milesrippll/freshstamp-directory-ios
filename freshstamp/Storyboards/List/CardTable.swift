//
//  CardTableViewController.swift
//  freshstamp
//
//  Created by Doug Chisholm on 18/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import AVFoundation
import StoreKit

class CardTableViewController: UITableViewController, ScanProtocol {
	
  fileprivate let max_countdown: Double = 60.0
  fileprivate var countdown: Double = 0.0
  fileprivate var timer: Timer!

  var loyaltyCard: LoyaltyCard!
	var searchBarVisible = true
	var filteredData = [FreshStamp]()
	var resultSearchController = UISearchController()

  var fsIdForEffect: Int = 0
  var imageCache = [String:UIImage]()

  var showCooldownAlert = false

  var showFullCardAlert = false

  var showNewCardAlert = false

  var showNewPointAlert = false


  @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
    if let swipeGesture = gesture as? UISwipeGestureRecognizer {
      switch swipeGesture.direction {
      case UISwipeGestureRecognizerDirection.right:
        print("Swiped right")
      case UISwipeGestureRecognizerDirection.down:
        print("Swiped down")
      case UISwipeGestureRecognizerDirection.left:
        print("Swiped left")
      case UISwipeGestureRecognizerDirection.up:
        print("Swiped up")
        self.openScanner()
      default:
        break
      }
    }
  }

  func Dismissed() {

    if showCooldownAlert {
      showCooldownAlert = false
			self.showAffectedCard() {
				self.present(self.cooldownAlert, animated: true, completion: nil)
			}
    }

    if showFullCardAlert {
      showFullCardAlert = false
			self.showAffectedCard() {
      	self.present(self.fullCardAlert, animated: true, completion: nil)
			}
    }

    if showNewCardAlert {
      showNewCardAlert = false
			self.showAffectedCard() {
				self.present(self.newCardAlert, animated: true, completion:nil)
			}
    }

    if showNewPointAlert {
      showNewPointAlert = false
			self.showAffectedCard() {
				self.present(self.newPointAlert, animated: true, completion: nil)
			}
    }

  }

	func showAffectedCard(completion: @escaping () -> Void) {

    if let rowIndex = CardManager.instance.myCards().index(where: { $0.id == fsIdForEffect }) {

      let indexPath = IndexPath.init(row: rowIndex, section: 0)

      self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)

      self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
			
			let fs = CardManager.instance.myCards()[rowIndex]
			
			let storyboard = UIStoryboard.init(name: "LoyaltyCard", bundle: nil)
			let viewController = storyboard.instantiateInitialViewController() as! LoyaltyCardViewController
			
			viewController.fsCard = fs
			viewController.loyalty = LoyaltyCard(data: fs)
			
			self.resultSearchController.isActive = false
			
			self.navigationController?.pushViewController(viewController, animated: true)
			
			completion()
    }
  }

  func CodeScanned(code: String) -> Bool {

    // card already exists
    if let fs = CardManager.instance.findCardInMyCards(code: code) {

      self.fsIdForEffect = fs.id

      var lc = LoyaltyCard.init(data: fs)

      if lc.canRedeem {
        showFullCardAlert = true
      } else if lc.checkin(code: code) {
        showNewPointAlert = true
      } else {
        showCooldownAlert = true
      }

      self.tableView.reloadData()

    } else if let fs = CardManager.instance.findCardInStore(code: code) {

      var lc = LoyaltyCard.init(data: fs)

      if lc.canRedeem {
        showFullCardAlert = true
      } else if lc.checkin(code: code) {
        showNewPointAlert = true
      } else {
        showCooldownAlert = true
      }
			self.fsIdForEffect = fs.id
      self.tableView.reloadData()
    } else {
      return false
    }
    return true
  }

  fileprivate lazy var cooldownAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.retry.description,
      message: Alert.cooldown.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.ok.description, style: UIAlertActionStyle.cancel, handler: nil))

    return alert
  }()

  fileprivate lazy var fullCardAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.full.description,
      message: Alert.fullRedeem.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.redeem.description,
        style: .default,
        handler: {
          action in self.displayCountdown()
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )
    return alert
  }()

  fileprivate lazy var newPointAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.point.description,
      message: Alert.newPoint.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.ok.description,
        style: .default ,
        handler: nil
      )
    )
    return alert
  }()

  fileprivate lazy var newCardAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.new.description,
      message: Alert.newCard.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.ok.description,
        style: .default,
        handler: nil
      )
    )
    return alert
  }()

  func displayCountdown() {

    countdown = 60.0

    if let card = CardManager.instance.myCards().first(where: { $0.id == fsIdForEffect } ) {
      var lc = LoyaltyCard.init(data: card)
      lc.redeem()
    }

    self.present(countdownAlert, animated: true, completion: nil)

    timer = Timer(
      timeInterval: 1.0,
      target: self,
      selector: #selector(CardTableViewController.refreshCountdown),
      userInfo: nil,
      repeats: true
    )

    RunLoop.current.add(timer, forMode: RunLoopMode.commonModes)

    Utils.delay(max_countdown, closure: {

      self.countdownAlert.dismiss(
        animated: true,
        completion: {
					
					self.timer.invalidate()
          self.tableView.reloadData()
					
					// ask for review
					if #available(iOS 10.3, *) {
						SKStoreReviewController.requestReview()
						
					} else {
						// Fallback on earlier versions
						// Try any other 3rd party or manual method here.
					}
			}
      )
    })

    timer.fire()
    refreshCountdown()

    loadingIndicator.startAnimating()
  }

  fileprivate lazy var redemptionAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.confirmation.description,
      message: Alert.redemption.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.redeem.description,
        style: .default,
        handler: {
          action in self.displayCountdown()
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )
    return alert
  }()

  fileprivate lazy var countdownAlert: UIAlertController = {

    var redemption = ""
    if let fsCard = CardManager.instance.myCards().first(where: { $0.id == fsIdForEffect }) {
      redemption = fsCard.card.redemptionCode
    }
    let alert = UIAlertController(title: "", message: "", preferredStyle: UIAlertControllerStyle.alert)
    alert.message = redemption

    let frame = CGRect(
      x: self.redemptionAlert.view.bounds.origin.x,
      y: self.redemptionAlert.view.bounds.origin.y,
      width: self.redemptionAlert.view.frame.width,
      height: self.redemptionAlert.view.frame.height + self.loadingIndicator.frame.height
    )

    alert.view.frame = frame

    alert.view.addSubview(self.loadingIndicator)

    return alert
  }()


  var loadingIndicator: UIActivityIndicatorView {

    let indicator = UIActivityIndicatorView(frame: redemptionAlert.view.bounds)

    indicator.center =  CGPoint(x: redemptionAlert.view.frame.width / 2.0, y: redemptionAlert.view.frame.height - indicator.frame.height)
    indicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]

    indicator.hidesWhenStopped = true
    indicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray

    return indicator
  }


  @objc func refreshCountdown() {

    countdownAlert.title = "\(Alert.countdown.description) \(Int(countdown)) seconds"

    if(countdown > 0){
      countdown -= 1
    }
  }

  fileprivate lazy var authorizationAlert: UIAlertController = {

    let alert = UIAlertController(
      title: Alert.disabled.description,
      message: Alert.authorization.description,
      preferredStyle: .alert
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.settings.description,
        style: .default,
        handler: {
          action in UIApplication.shared.openURL(URL.init(string: UIApplicationOpenSettingsURLString)!)
      }
      )
    )

    alert.addAction(
      UIAlertAction(
        title: Alert.cancel.description,
        style: .cancel,
        handler: nil
      )
    )

    return alert
  }()

  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .lightContent
  }

  func hasCameraPermission() -> Bool {

    let cameraMediaType = AVMediaType.video
    let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: cameraMediaType)

    switch cameraAuthorizationStatus {
    case .denied: return false
    case .authorized: return true
    case .restricted: return false

    case .notDetermined:
      return true
    }
  }
	
	
	
  func openScanner() {
    scannerVC.modalPresentationStyle = .overCurrentContext

		
		if !UserDefaults.standard.bool(forKey: "hasRequestedAlwaysOn") {
			
			Geowave.requestLocationAlwaysAuthorization()
			UserDefaults.standard.set(true, forKey: "hasRequestedAlwaysOn")
			UserDefaults.standard.synchronize()
		} else {
			
			if ![CLAuthorizationStatus.authorizedAlways].contains(CLLocationManager.authorizationStatus()) {
				self.present(self.authorizationAlert, animated: true)
			} else {

				if !hasCameraPermission() {
					self.present(self.authorizationAlert, animated: true)
				} else {
					self.present(scannerVC, animated: true, completion: nil)
				}
			}
		}
  }

  lazy var scannerVC: ScannerViewController = {

    let storyboard = UIStoryboard.init(name: "Scanner", bundle: nil)

    let scannerVC: ScannerViewController = storyboard.instantiateInitialViewController() as! ScannerViewController

    scannerVC.delegate = self

    return scannerVC
  }()

  var imageStore = ImageStore("CardListImageCache")

  override func viewWillAppear(_ animated: Bool) {
		
		self.navigationItem.title = "FreshStamp"
		
		if searchBarVisible {
			self.tableView.setContentOffset(CGPoint.init(x: 0, y: (self.tableView.tableHeaderView?.frame.size.height)! ), animated: true)
			searchBarVisible = false
		}
  }

  override func viewDidAppear(_ animated: Bool) {
		
		if !UserDefaults.standard.bool(forKey: "hasSeenPrivacy") {
			
			let alert = UIAlertController.init(title: "Privacy Information", message:
				"This app analyses your location data to verify store visits."
				+ "\n "
				+ "\nBy giving consent for us to verify store visits your data is returned to our servers for anonymous analysis."
				+ "\nWe are able to keep this app free because of this analysis "
				+ "\nThank you for your support."
				+ "\n"
					//+ "\nFacebook - Advertising Identifier"
				+ "\n"
				//	"Learn more about these companies.", style: .default, handler: { action in self.showOptinScreen() }
				, preferredStyle: .alert)
			
			let optinButton = UIAlertAction.init(title: "Accept", style: .default, handler: { _ in
				GeowaveService.Optin(allow: true)	
				GeowaveService.TrackOnboardDetails(optedInAt: 1)
			})
			
			let dismissButton = UIAlertAction.init(title: "Decline", style: .default, handler: { _ in
				GeowaveService.Optin(allow: false)
				
			})
			
							
			//let moreinfoButton = UIAlertAction.init(title: "Learn more about these companies.", style: .default, handler: { action in self.showOptinScreen() } )
			
			
			alert.addAction(dismissButton)
            alert.addAction(optinButton)
			
			self.present(alert, animated: true, completion: {UserDefaults.standard.set(true, forKey: "hasSeenPrivacy")
				UserDefaults.standard.synchronize()
			})
		}
  }
	
	func showOptinScreen() {
		let vc = UIStoryboard(name: "PrivacyOptin", bundle: nil).instantiateViewController(withIdentifier: "PrivacyOptin")
		self.navigationController?.present(vc, animated: true, completion: nil)
	}
	
	func showInfoScreen() {
		let vc = UIStoryboard(name: "PrivacyOptin", bundle: nil).instantiateInitialViewController()
		self.navigationController?.pushViewController(vc!, animated: true)
	}
	
	@IBAction func showScanner(_ sender: Any) {
		self.openScanner()
	}
	
	func toggleSearchBar() {
		
		UIView.animate(withDuration: 0.2) {
			if !self.searchBarVisible {
				self.tableView.tableHeaderView = self.resultSearchController.searchBar
				self.tableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
				self.searchBarVisible = true
			} else {
				self.tableView.tableHeaderView = nil
				self.searchBarVisible = false
			}
		}
	}

  func shouldShowOnboarding() -> Bool {
    
    return false
    
    let key = "OnBoardingDone"
    let defaults = UserDefaults.standard

    let hasSeen = defaults.bool(forKey: key)

    defaults.setValue(true, forKey: key)

    return !hasSeen
  }
	@IBAction func displayPrivacyPolicy(_ sender: Any) {
			self.showInfoScreen()
	}
	
  override func viewDidLoad() {
		
		self.resultSearchController = ({
			let controller = UISearchController(searchResultsController: nil)
			controller.searchResultsUpdater = self
			controller.dimsBackgroundDuringPresentation = false
			controller.searchBar.sizeToFit()
			
			// cancel text
			controller.searchBar.tintColor = UIColor(hexString: "70C144")
			controller.searchBar.barTintColor = UIColor(hexString: "F9FAFC")
			
			controller.searchBar.layer.borderWidth = 1
			controller.searchBar.layer.borderColor = UIColor(hexString: "F9FAFC").cgColor
			controller.hidesNavigationBarDuringPresentation = false
			
			self.tableView.tableHeaderView = controller.searchBar
			
			UIApplication.shared.statusBarStyle = .lightContent //or .default
            
			setNeedsStatusBarAppearanceUpdate()
			
			self.tabBarController?.tabBar.unselectedItemTintColor = UIColor(red: 36/255, green: 122/255, blue: 0/255, alpha: 1.0) /* #247a00 */
			
			return controller
		})()
		
		if shouldShowOnboarding() {
			
			let storyboard = UIStoryboard.init(name: "Onboarding", bundle: nil)
      let onboardingVC = storyboard.instantiateInitialViewController() as! OnboardingPageViewController
      self.present(onboardingVC, animated: true, completion: nil)
			
			Geowave.requestLocationWhenInUseAuthorization()
			
		}
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		let fs: FreshStamp!
		
		if (self.resultSearchController.isActive) {
			fs = filteredData[indexPath.row]
		}
		else {
			fs = CardManager.instance.myCards()[indexPath.row]
		}

    let cell = tableView.dequeueReusableCell(withIdentifier: "cardCellId", for: indexPath) as! CardCellView

    var lc = LoyaltyCard.init(data: fs)

    cell.name.text  = fs.business.name
    cell.score.text = "\(lc.checkins)/\(lc.stamps)"

    switch(fs.card.stampTheme)
    {
    case "coffee":
      cell.stamp.image = #imageLiteral(resourceName: "coffee_stamp_checked")
      break
    case "heart":
      cell.stamp.image = #imageLiteral(resourceName: "heart_stamp_checked")
      break
    case "barber":
      cell.stamp.image = #imageLiteral(resourceName: "barber_stamp_checked")
      break
    default:
      cell.stamp.image = #imageLiteral(resourceName: "default_stamp_checked")
    }

    // default image for card
    cell.logo.contentMode =  .scaleAspectFit
    cell.logo.backgroundColor = UIColor(hexString: fs.business.backgroundColor)

    let urlString = "https://geowaveapi.blob.core.windows.net/freshstamp/\(fs.business.logoUrl)"

    if let img = imageCache[urlString] {
      cell.logo.image = img
    }
    else {

      if let data = imageStore.getValue(for: "\(fs.id)") {
        let image = UIImage(data: data)
        cell.logo.image = image
        self.imageCache[urlString] = image
      } else {

        if let url = URL(string: urlString) {

          let task =
            URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in

              if error == nil {
                // Convert the downloaded data in to a UIImage object
                let image = UIImage(data: data!)

                self.imageStore.set(value: data, for: "\(fs.id)")

                // Store the image in to our cache
                self.imageCache[urlString] = image
                // Update the cell
                DispatchQueue.main.async {
                  if let cellToUpdate = tableView.cellForRow(at: indexPath) {
                    let cardCell = cellToUpdate as! CardCellView
                    cardCell.logo.image = image
                  }
                }
              }
            })
          task.resume()
        }
      }
    }
    return cell
  }
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

		if (self.resultSearchController.isActive) {
			return self.filteredData.count
		}
		else {
			return CardManager.instance.myCards().count
		}
  }

  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		let fs: FreshStamp!
		
		if (self.resultSearchController.isActive) {
			fs = filteredData[indexPath.row]
		}
		else {
			fs = CardManager.instance.myCards()[indexPath.row]
		}

    if fs.id == 0 {
      openScanner()
    } else {

      let storyboard = UIStoryboard.init(name: "LoyaltyCard", bundle: nil)
      let viewController = storyboard.instantiateInitialViewController() as! LoyaltyCardViewController


      viewController.fsCard = fs
      viewController.loyalty = LoyaltyCard(data: fs)
			
			self.resultSearchController.isActive = false
			
      self.navigationController?.pushViewController(viewController, animated: true)
    }
  }
}

extension CardTableViewController : UISearchResultsUpdating {
	
	func updateSearchResults(for searchController: UISearchController) {
		
		filteredData.removeAll(keepingCapacity: false)
		
		filteredData = CardManager.instance.allCards().filter({ (f) -> Bool in
			f.business.name.lowercased().contains(searchController.searchBar.text!.lowercased())
		})
		
		self.tableView.reloadData()
	}
}

enum ButtonAction : String {

  case redeem         = "Redeem"
  case scan           = "Scan"
  case cooldown       = "Checkin again later"
  case authorization  = "Authorize"

  var title: String {
    return self.rawValue
  }
}

enum Alert : String {

  case ok             = "Ok"
  case new            = "New Card added"
  case newCard        = "You just added a new card to your Wallet and collected your first point!"
  case confirmation   = "Are you sure?"
  case back           = "Back"
  case redemption     = "Redeeming this offer will display a redemption code for 60 seconds which you must show to a member of staff"
  case redeem         = "Redeem"
  case cancel         = "Cancel"
  case point          = "Loyalty Point Added!"
  case newPoint       = "Successfully added a new loyalty point to your card"
  case disabled       = "Loyalty Card Disabled"
  case retry          = "Come back later"
  case cooldown       = "You've recently checked in. Please try again later."
  case countdown      = "Please redeem this code in the next"
  case full           = "Your Loyalty Card is complete."
  case fullRedeem     = "Press redeem to collect your reward."
  case authorization  = "The Loyalty Card requires the use of your Camera to scan QR Codes and 'Always On' location to check your store visits, please enable this in the app settings"
  case settings       = "Settings"

  var description: String {
    return self.rawValue
  }
}

