//
//  CardCellView.swift
//  freshstamp
//
//  Created by Doug Chisholm on 18/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class CardCellView : UITableViewCell {

  //@IBOutlet weak var card: UIView!
  @IBOutlet weak var logo: UIImageView!
  @IBOutlet weak var stamp: UIImageView!
  @IBOutlet weak var score: UILabel!
  @IBOutlet weak var name: UILabel!
  @IBOutlet weak var scoreBackground: UIView!

  var cornerRadius: CGFloat = 6

  var shadowOffsetWidth: Int = 0
  var shadowOffsetHeight: Int = 6
  var shadowColor: UIColor? = UIColor.init(hexString: "8392A7").withAlphaComponent(0.2)
  var shadowOpacity: Float = 0.7
}
