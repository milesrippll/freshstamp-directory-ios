//
//  StampView.swift
//  freshstamp
//
//  Created by Doug Chisholm on 23/11/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class StampView: CardView {
	
	var fsCard: FreshStamp!
	var loyaltyCard: LoyaltyCard!
	
	override func layoutSubviews() {
		super.layoutSubviews()
	}
	
	func refresh() {
		
		var noOfRows = 0
		var rowIndex = 0
		var stampsChecked = 0
		var rowStamps: [Int] = [Int]()
		var rowCheckins: [Int] = [Int]()
		
		// remove all subviews
		for view in self.subviews {
			view.removeFromSuperview()
		}
		
		// determine number of rows
		if self.loyaltyCard.stamps <= 3 {
			noOfRows = 1
		} else if self.loyaltyCard.stamps <= 10 {
			noOfRows =  2
		} else {
			noOfRows = 3
		}
		
		// append stamps unchecked and checked
		for _ in 0..<noOfRows {
			rowStamps.append(0)
			rowCheckins.append(0)
		}
		
		// applies stamps
		for _ in 0..<loyaltyCard.stamps {
			rowStamps[rowIndex] += 1
			rowIndex += 1
			rowIndex = rowIndex == noOfRows ? 0 : rowIndex //resets row
		}
		
		// applies checkins
		for i in 0..<noOfRows {
			for _ in 0..<rowStamps[i] {
				if stampsChecked < loyaltyCard.checkins {
					rowCheckins[i] += 1
					stampsChecked += 1
				}
			}
		}
		
		//stamp width: size & inset
		let loyalty_card_width = self.frame.width       // loyalty card image stretches across screen
		var stamp_frame_width = loyalty_card_width * 0.8 // use 80% to avoid drawing over card borders - can be ajusted
		
		if loyaltyCard.stamps < 3 {
			stamp_frame_width = loyalty_card_width * 0.6
		}
		
		let stamp_frame_width_inset = (loyalty_card_width - stamp_frame_width) / CGFloat(2.0)   // calculate inset
		
		//stamp height: size & inset
		let loyalty_card_height = self.frame.height
		let stamp_frame_height = (loyalty_card_height * 0.9) / CGFloat(noOfRows) // use 90% to avoid drawing over card borders - can be ajusted
		let stamp_frame_height_inset = (loyalty_card_height - (stamp_frame_height * CGFloat(noOfRows))) / CGFloat(2.0)  // calculate inset
		
		//max stamp size & margins
		
		// x
		let max_margin_x = 0.005 * stamp_frame_width // 0.5% of the total frame width
		// (total frame width - (all margins)) / total stamps in first row (row with the most possible stamps)
		let max_stamp_x = (stamp_frame_width - ( (CGFloat(rowStamps[0] - 1) * max_margin_x ))) / CGFloat(rowStamps[0])
		
		// y
		let max_margin_y = 0.02 * stamp_frame_height // 2% of the total frame height
		let max_stamp_y = stamp_frame_height - (2 * max_margin_y) // total frame size - (all row margins) / total stamps in first row
		
		// stamp size and margin
		let stamp_max_size = max_stamp_x > max_stamp_y ? max_stamp_y : max_stamp_x
		let stamp_margin_x = (stamp_frame_width  - ( CGFloat(rowStamps[0]) * stamp_max_size )) / CGFloat(rowStamps[0]-1)
		let stamp_margin_y = (stamp_frame_height - stamp_max_size) / 2.0
		
		// animation delay
		var delay: Double = 0.1
		
		for i in 0..<noOfRows {
			
			// stamp row frame
			let stamp_frame = UIView.init(frame: CGRect.init(origin: CGPoint.init(x: stamp_frame_width_inset, y: stamp_frame_height_inset + ( CGFloat(i) * stamp_frame_height )), size: CGSize.init(width: stamp_frame_width, height: stamp_frame_height)))
			
			// row inset based on number of stamps to center in frame
			let stamp_inset_x = ( stamp_frame_width - ( ( CGFloat(rowStamps[i]) * stamp_max_size )  + ( CGFloat(rowStamps[i]-1) * stamp_margin_x ) ) ) / 2.0
			
			for x in 0..<rowStamps[i] {
				
				// calculate stamp position
				var stamp_origin = CGPoint.zero
				
				stamp_origin.x += stamp_inset_x + (CGFloat(x) * stamp_max_size) + (CGFloat(x) * stamp_margin_x)
				
				stamp_origin.y += stamp_margin_y
				
				// stamp image
				let img = UIImageView.init(frame: CGRect.init(origin: stamp_origin, size: CGSize.init(width: stamp_max_size, height: stamp_max_size)))
				
				img.alpha = 0
				
				// pick image according to stamp theme
				if x < rowCheckins[i] {
					switch(self.fsCard.card.stampTheme)
					{
					case "coffee":
						img.image = #imageLiteral(resourceName: "coffee_stamp_checked")
						break
					case "heart":
						img.image = #imageLiteral(resourceName: "heart_stamp_checked")
						break
					case "barber":
						img.image = #imageLiteral(resourceName: "barber_stamp_checked")
						break
					default:
						img.image = #imageLiteral(resourceName: "default_stamp_checked")
					}
				} else {
					switch(self.fsCard.card.stampTheme)
					{
					case "coffee":
						img.image = #imageLiteral(resourceName: "coffee_stamp_unchecked")
						break
					case "heart":
						img.image = #imageLiteral(resourceName: "heart_stamp_unchecked")
						break
					case "barber":
						img.image = #imageLiteral(resourceName: "barber_stamp_unchecked")
						break
					default:
						img.image = #imageLiteral(resourceName: "default_stamp_unchecked")
					}
				}
				
				// add stamp image
				stamp_frame.addSubview(img)
				
				// animate display
				UIView.animate(withDuration: 0.7, delay: delay, usingSpringWithDamping: 0.3, initialSpringVelocity: 0.1, options: .curveEaseIn, animations: {
					img.alpha = 1.0
				}, completion: nil)
				delay += 0.1
			}
			
			// add stamp frame to view
			self.addSubview(stamp_frame)
		}
	}
}

