//
//  CardView.swift
//  freshstamp
//
//  Created by Doug Chisholm on 16/11/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class CardView: UIView {

  var cornerRadius: CGFloat = 6.0

  var shadowOffsetWidth: Double = 0.0
  var shadowOffsetHeight: Double = 3.2

  var shadowOpacity: Float = 0.2
  var shadowColor: UIColor = UIColor.init(hexString: "8392A7")
	
	var image: UIImage!
	
  override func layoutSubviews() {

    self.layer.cornerRadius = cornerRadius
    self.layer.masksToBounds = false

    let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius)
    self.layer.shadowColor = shadowColor.cgColor
    self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
    self.layer.shadowOpacity = shadowOpacity
    self.layer.shadowPath = shadowPath.cgPath
		
		let backgroundImage = UIImageView(image: image)
		backgroundImage.contentMode = .scaleAspectFill
		backgroundImage.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
		backgroundImage.alpha = 0.1
		backgroundImage.clipsToBounds = true
		
		self.addSubview(backgroundImage)
		self.sendSubview(toBack: backgroundImage)
  }
}
