//
//  CardImageView.swift
//  freshstamp
//
//  Created by Doug Chisholm on 16/11/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class CardImageView: UIImageView {

  var cornerRadius: CGFloat = 6.0

  override func layoutSubviews() {

    self.layer.cornerRadius = cornerRadius
    self.layer.masksToBounds = true
  }
}
