//
//  ButtonView.swift
//  freshstamp
//
//  Created by Doug Chisholm on 23/11/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation
import UIKit

class ButtonView: UIButton {

  var cornerRadius: CGFloat = 22.0

  var shadowOffsetWidth: Double = 0.0
  var shadowOffsetHeight: Double = 3.2

  var shadowOpacity: Float = 0.2
  var shadowColor: UIColor = UIColor.init(hexString: "8392A7")

  override func layoutSubviews() {
    super.layoutSubviews()
    self.backgroundColor = .white
    self.layer.cornerRadius = cornerRadius
    self.layer.masksToBounds = false

    let shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: cornerRadius)
    self.layer.shadowColor = shadowColor.cgColor
    self.layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
    self.layer.shadowOpacity = shadowOpacity
    self.layer.shadowPath = shadowPath.cgPath
		
		self.backgroundColor = UIColor(hexString: "70C144")
		
		self.setTitle((self.titleLabel?.text)!)
  }

  func setTitle(_ text: String) {
    if let titleFont = UIFont(name: "montserrat-bold", size: 12.0)  {

      let attributes: [NSAttributedStringKey : Any] = [
        NSAttributedStringKey.font : titleFont,
        NSAttributedStringKey.foregroundColor : UIColor.white
			]

      self.setAttributedTitle(NSAttributedString(string: text, attributes: attributes), for: UIControlState.normal)
    }
  }
}
