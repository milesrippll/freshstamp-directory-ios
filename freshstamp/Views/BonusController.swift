//
//  SecondViewController.swift
//  WeatherBot
//
//  Created by Doug Chisholm on 16/02/2019.
//  Copyright © 2019 Rippll. All rights reserved.
//

import UIKit

class BonusController: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var pagerControl: UIPageControl!
    @IBOutlet weak var scrollView: UIScrollView!
    var slidesCreated : Bool = false
    
    func createSlides() -> [UIView] {
        
        let slideOnboarding1 : BonusSlide = Bundle.main.loadNibNamed("BonusSlide", owner: self, options: nil)!.first as! BonusSlide
       
        slideOnboarding1.configure(itemNumber: 1)
        
        let slideOnboarding2 : BonusSlide = Bundle.main.loadNibNamed("BonusSlide", owner: self, options: nil)!.first as! BonusSlide
        
        slideOnboarding2.configure(itemNumber: 2)
        
        let slideOnboarding3 : BonusSlide = Bundle.main.loadNibNamed("BonusSlide", owner: self, options: nil)!.first as! BonusSlide
        
        slideOnboarding3.configure(itemNumber: 3)
        
        slidesCreated = true
        
        return [slideOnboarding1, slideOnboarding2, slideOnboarding3]
        
    }
    
    func setUpSlides(slides : [UIView]) {
        
        let viewWidth : CGFloat = self.view.frame.width
        let viewHeight : CGFloat = self.view.frame.height
        
        scrollView.frame = CGRect(x: 0, y: 0, width: viewWidth, height: viewHeight)
        
        scrollView.contentSize = CGSize(width: viewWidth * CGFloat(slides.count), height: viewHeight)
        
        scrollView.isPagingEnabled = true
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: viewWidth, height: viewHeight)
            
            scrollView.addSubview(slides[i])
        }
        
        
        
    }
    
    @IBAction func enableAI(sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("askForNotifications"), object: nil)
    }
    
    @objc func gotoForecast(notification: Notification)  {
        tabBarController?.selectedIndex = 0
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageIndex = round(scrollView.contentOffset.x / view.frame.width)
        pagerControl.currentPage = Int(pageIndex)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.gotoForecast(notification:)), name: Notification.Name("gotoForecast"), object: nil)
        
        scrollView.delegate = self
        scrollView.alwaysBounceVertical = false
        
        if (!self.slidesCreated) {
            let slides = createSlides()
            setUpSlides(slides: slides)
            pagerControl.numberOfPages = slides.count
            pagerControl.currentPage = 0
            view.bringSubview(toFront: pagerControl)
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
     //   GeowaveService.TrackOnboardDetails(optedInAt: 11)
        
    }
    
    
}

