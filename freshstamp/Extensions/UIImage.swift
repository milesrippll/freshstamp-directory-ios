//
//  UIImageExtension.swift
//  appsplash
//
//  Created by Doug Chisholm on 01/01/2016.
//  Copyright © 2016 Doug Chisholm. All rights reserved.
//

import UIKit

extension UIImage {

  static func resize(image: UIImage, width: CGFloat) -> UIImage {

    let scaleFactor: CGFloat = width / image.size.height;

    let newHeight: CGFloat = image.size.height * scaleFactor;
    let newWidth: CGFloat = image.size.width * scaleFactor;

    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight));
    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
    UIGraphicsEndImageContext();

    return newImage;
  }

  static func resize(image: UIImage, height: CGFloat) -> UIImage {

    let oldHeight: CGFloat = image.size.height;
    let scaleFactor: CGFloat = height / oldHeight;

    let newHeight: CGFloat = image.size.height * scaleFactor;
    let newWidth: CGFloat = image.size.width * scaleFactor;

    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight));
    image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
    UIGraphicsEndImageContext();

    return newImage;
  }

  static func image(with color: UIColor, and size: CGSize) -> UIImage {

    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)

    UIGraphicsBeginImageContextWithOptions(size, false, 0)

    color.setFill()
    UIRectFill(rect)

    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!

    UIGraphicsEndImageContext()

    return image
  }

  func with(tint color: UIColor) -> UIImage {

    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)

    let context = UIGraphicsGetCurrentContext()

    context!.translateBy(x: 0, y: self.size.height)
    context!.scaleBy(x: 1.0, y: -1.0)
    context!.setBlendMode(CGBlendMode.normal)

    let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect

    context!.clip(to: rect, mask: self.cgImage!)
    color.setFill()
    context!.fill(rect)

    let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage

    UIGraphicsEndImageContext()

    return newImage
  }

  class func image(with color:UIColor?) -> UIImage! {

    let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0);

    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)

    let context = UIGraphicsGetCurrentContext();

    if let color = color {
      color.setFill()
    } else {
      UIColor.white.setFill()
    }

    context!.fill(rect)

    let image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
  }
}
