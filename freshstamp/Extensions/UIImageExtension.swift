//
//  UIImageExtension.swift
//  appsplash
//
//  Created by Doug Chisholm on 01/01/2016.
//  Copyright © 2016 Doug Chisholm. All rights reserved.
//

import UIKit

extension UIImage {

  static func resizeImageToWidth(sourceImage: UIImage, toWidth: CGFloat) -> UIImage {

    let scaleFactor: CGFloat = toWidth / sourceImage.size.height;

    let newHeight: CGFloat = sourceImage.size.height * scaleFactor;
    let newWidth: CGFloat = sourceImage.size.width * scaleFactor;

    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight));
    sourceImage.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
    UIGraphicsEndImageContext();

    return newImage;
  }

  static func resizeImageToHeight(sourceImage: UIImage, toHeight: CGFloat) -> UIImage {

    let oldHeight: CGFloat = sourceImage.size.height;
    let scaleFactor: CGFloat = toHeight / oldHeight;

    let newHeight: CGFloat = sourceImage.size.height * scaleFactor;
    let newWidth: CGFloat = sourceImage.size.width * scaleFactor;

    UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight));
    sourceImage.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
    let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
    UIGraphicsEndImageContext();

    return newImage;
  }

  static func imageWithColor(color: UIColor, size: CGSize) -> UIImage {

    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)

    UIGraphicsBeginImageContextWithOptions(size, false, 0)

    color.setFill()
    UIRectFill(rect)

    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!

    UIGraphicsEndImageContext()

    return image
  }

  func applyTintColor(tintColor: UIColor) -> UIImage {

    UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)

    let context = UIGraphicsGetCurrentContext()

    context!.translateBy(x: 0, y: self.size.height)
    context!.scaleBy(x: 1.0, y: -1.0)
    context!.setBlendMode(CGBlendMode.normal)

    let rect = CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height) as CGRect

    context!.clip(to: rect, mask: self.cgImage!)
    tintColor.setFill()
    context!.fill(rect)

    let newImage = UIGraphicsGetImageFromCurrentImageContext()! as UIImage

    UIGraphicsEndImageContext()

    return newImage
  }

  class func imageWithColor(color:UIColor?) -> UIImage! {

    let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0);

    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)

    let context = UIGraphicsGetCurrentContext();

    if let color = color {

      color.setFill()
    }
    else {

      UIColor.white.setFill()
    }

    context!.fill(rect)


    let image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return image;
  }
}
