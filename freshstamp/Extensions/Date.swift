//
//  Date.swift
//  appsplash
//
//  Created by Doug Chisholm on 10/03/2016.
//  Copyright © 2016 Doug Chisholm. All rights reserved.
//

import Foundation

extension Date {

  func isGreaterThanDate(_ dateToCompare: Date) -> Bool {
    return self.compare(dateToCompare) == ComparisonResult.orderedDescending
  }
  
  func isLessThanDate(_ dateToCompare: Date) -> Bool {
    return self.compare(dateToCompare) == ComparisonResult.orderedAscending
  }
  
  func equalToDate(_ dateToCompare: Date) -> Bool {
    return self.compare(dateToCompare) == ComparisonResult.orderedSame
  }
  
  func addDays(_ daysToAdd: Int) -> Date {
    return self.addingTimeInterval(Double(daysToAdd) * 60 * 60 * 24)
  }
  
  func addHours(_ hoursToAdd: Int) -> Date {
    return self.addingTimeInterval(Double(hoursToAdd) * 60 * 60)
  }
}
