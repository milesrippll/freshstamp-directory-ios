//
//  ColorExtension.swift
//  appsplash
//
//  Created by Doug Chisholm on 30/12/2015.
//  Copyright © 2015 Doug Chisholm. All rights reserved.
//

import UIKit

extension UIColor {

  convenience init(hexString: String) {

    let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)

    var int = UInt32()

    Scanner.init(string: hex).scanHexInt32(&int)

    let a, r, g, b: UInt32

    switch hex.count {

      case 3: // RGB (12-bit)
        (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
      case 6: // RGB (24-bit)
        (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
      case 8: // ARGB (32-bit)
        (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
      default:
        (a, r, g, b) = (1, 1, 1, 0)
    }

    self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
  }

  func isLight() -> Bool
  {

    let components = self.cgColor.components

    let brightness = ((((components?[0])! * 299.0) as CGFloat) + (((components?[1])! * 587.0) as CGFloat) + (((components?[2])! * 114.0)) as CGFloat) / (1000.0 as CGFloat)

    if brightness < 0.5
    {
      return false
    }
    else
    {
      return true
    }
  }
}
