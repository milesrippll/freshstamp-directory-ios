//
//  ImageStore.swift
//  freshstamp
//
//  Created by Doug Chisholm on 20/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//
import Foundation
import UIKit

class ImageStore: StoreProtocol {

  let fileManager = FileManager.default
  let paths = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as String

  let storeKey: String
  
  func set(value: Data?, for key: String) {

    if let data = value {

      let filePathToWrite = "\(paths)/\(storeKey)"
      let path = "\(filePathToWrite)_\(key).png"

      if fileManager.createFile(atPath: path, contents: data, attributes: nil) {

        if fileManager.fileExists(atPath: path) {
          print("ImageStore: file created at creation \(path)")
        }
      } else {
        print("ImageStore: could not write file at creation \(path)")
      }
    }
  }
  
  func getValue(for key: String) -> Data? {

    let filePathToWrite = "\(paths)/\(storeKey)"
    let path = "\(filePathToWrite)_\(key).png"

    if fileManager.fileExists(atPath: path) {
      print("ImageStore: found file at location \(path)")

      if let fileAttributes = try? fileManager.attributesOfItem(atPath: path),
        let fileDate = fileAttributes[FileAttributeKey.creationDate] as? Date {

          if fileDate < Date().addDays(-1) {

            print("ImageStore: cache expired for file at location \(path)")
            try? fileManager.removeItem(atPath: path)
            return nil
          }
      }

      return fileManager.contents(atPath: path)
    }
    return nil
  }
	
	func clearCache(for keys: [Int]) {
		
		for key in keys {
			let filePathToWrite = "\(paths)/\(storeKey)"
			let path = "\(filePathToWrite)_\(key).png"
			
			if fileManager.fileExists(atPath: path) {
				print("ImageStore: found file at location \(path)")
				
				if (try? fileManager.attributesOfItem(atPath: path)) != nil {
						try? fileManager.removeItem(atPath: path)
					}
				}
			}
	}
  
  required init(_ storeKey: String) {
    self.storeKey = storeKey
  }
  
  typealias ValueType = Data?
}
