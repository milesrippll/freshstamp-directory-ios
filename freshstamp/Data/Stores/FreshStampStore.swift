//
//  FreshStampStore.swift
//  freshstamp
//
//  Created by Doug Chisholm on 20/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation

class FreshStampStore {
  
  private var cardStore = [FreshStamp]()
  
  private init() { }
  
  static let instance: FreshStampStore = {
    
    let instance = FreshStampStore()

    return instance
  }()

  func loadFromFile() {

    let fileName = "LoyaltyCards"
    let fileExt = "json"

    let path = Bundle.main.path(forResource: fileName, ofType: fileExt)
    let url = URL(fileURLWithPath: path!)

    if let data = try? Data(contentsOf: url, options: Data.ReadingOptions.alwaysMapped) {
      do {
        let cachedStore = try JSONDecoder().decode([FreshStamp].self, from: data)
        self.cardStore = cachedStore
      }
      catch {
        print("FreshStampStore: error loading file: \(error)")
      }
    }
  }

  func refresh() {
    self.cardsFromAPI() { (cards, error) in
      if error == nil,
        let cards = cards {
        self.cardStore = cards
        CardManager.instance.refreshMyCardsFromAPI()
        print("FreshStampStore: Refresh finished")
      } else {
        print("FreshStampStore: Error refreshing")
      }
    }
  }
  
  func cards() -> [FreshStamp] {
    return self.cardStore
  }

  enum BackendError: Error {
    case urlError(reason: String)
    case objectSerialization(reason: String)
  }

  func cardsFromAPI(completionHandler: @escaping ([FreshStamp]?, Error?) -> Void) {

    let endpoint = "https://stag-freshstamp.azurewebsites.net/api/freshstampdirectory"

    guard let url = URL(string: endpoint) else {
      print("API: cannot construct URL")
      let error = BackendError.urlError(reason: "Could not construct URL")
      completionHandler(nil, error)
      return
    }

    var urlRequest = URLRequest(url: url)

    urlRequest.httpMethod = "GET"
    urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
    let session = URLSession.shared

    let task = session.dataTask(with: urlRequest) {
      (data, response, error) in
      guard let responseData = data else {
        print("API: No data received")
        completionHandler(nil, error)
        return
      }
      guard error == nil else {
        print("API: Error received")
        completionHandler(nil, error)
        return
      }

      let decoder = JSONDecoder()
      do {
        let cards = try decoder.decode([FreshStamp].self, from: responseData)
        completionHandler(cards, nil)
      } catch {
        print("API: Invalid JSON Format")
        completionHandler(nil, error)
      }
    }
    task.resume()
    }
}

