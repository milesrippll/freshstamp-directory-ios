//
//  CardStore.swift
//  freshstamp
//
//  Created by Tomasz Walis-Walisiak on 22/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation

class CardStore: StoreProtocol {

  struct CardData: Codable{
    let id: Int 
    var numberOfCheckIns: Int = 0
    var lastCheckIn: Date = Date.distantPast
    var oneOffUsed: Bool! = false
  }

  let storeKey: String
  
  func set(value: CardData?, for key: String) {
    let defaults = UserDefaults.standard
    do {
      try defaults.setValue(PropertyListEncoder().encode(value), forKey: "\(storeKey)\(key)")
    } catch {
      print("CardStore: Could not set value for store \(storeKey) with key \(key)")
    }
  }
  
  func getValue(for key: String) -> CardData? {
    let defaults = UserDefaults.standard
    if let data = defaults.object( forKey: "\(storeKey)\(key)") as? Data {
      
      do {
        return try PropertyListDecoder().decode(CardData.self, from: data)
      } catch {
        print("CardStore: Could not load value for store \(storeKey) with key \(key)")
      }
    } else {
      print("CardStore: Could not find value for store \(storeKey) with key \(key)")
    }
    return nil
  }
  
  required init(_ storeKey: String) {
    self.storeKey = storeKey
  }
  
  typealias ValueType = CardData?
}
