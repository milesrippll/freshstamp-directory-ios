//
//  Freshstamp.swift
//  freshstamp
//
//  Created by Doug Chisholm on 20/09/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation

struct FreshStamp: Codable {
  let id: Int
  let card: Card
  let business: Business
}

struct Business: Codable {
  let name: String
  let logoUrl: String
  let webUrl: String
  let backgroundColor: String
  let yelpUrl: String!
}

struct Card: Codable {

  let offer: String
  let code: String
  let masterCode: String
  let onetimeCode: String!
  let redemptionCode: String
  let stamps: Int
  let cooldown: Int
  let stampTheme: String
  let backgroundColor: String
  let textColor: String
  let butonTextColor: String
  let butonColor: String
}

