//
//  AppSettings.swift
//  stampIt2
//
//  Created by Doug Chisholm on 06/12/2016.
//  Copyright © 2016 rippll. All rights reserved.
//

import Foundation

class Settings {

  class func get(key: String) -> AnyObject {
    return Bundle.main.object(forInfoDictionaryKey: key)! as AnyObject
  }
}

enum SettingsKeys: String {

  var description: String {
    return self.rawValue
  }
  
  case global_StampItVersion = "StampItVersion"
  case global_FreshStampVersion = "FreshStampVersion"
  case global_AppName = "AppName"
  case global_DisplayAppName = "CFBundleName"
  case global_BundleID = "CFBundleIdentifier"
  case global_GeowaveAppID = "GeowaveAppID"
  case global_Version = "CFBundleShortVersionString"
  case global_VersionCode = "CFBundleVersion"
  case global_AppUrl = "AppURL"
  case global_OrderBooking = "OrderBooking"
  case ios_SKU = "iOSSKU"
  case color_Interface = "ColorInterface"
  case color_InterfaceText = "ColorInterfaceText"
  case color_TabBar = "ColorTabBar"
  case color_TabBarActive = "ColorTabBarActive"
  case color_HomeBackground = "ColorHomeBackground"
  case color_LoyaltyBackground = "ColorLoyaltyBackground"
  case color_LoyaltyText = "ColorLoyaltyText"
  case color_LoyaltyButton = "ColorLoyaltyButton"
  case color_LoyaltyButtonText = "ColorLoyaltyButtonText"
  case loyalty_OfferText = "LoyaltyOfferText"
  case loyalty_QRCode = "LoyaltyQRCode"
  case loyalty_QRCodeOneOff = "LoyaltyQRCodeOneOff"
  case loyalty_MasterQRCode = "LoyaltyMasterQRCode"
  case loyalty_RedemptionText = "LoyaltyRedemptionText"
  case loyalty_NoOfStamps = "LoyaltyNoOfStamps"
  case loyalty_CheckinTime = "LoyaltyTimeBetweenCheckins"
  case map_Places = "MapPlaces"
  case map_PlaceAddress = "MapPlaceAddress"
  case map_PlaceName = "MapPlaceName"
  case map_PlacePhone = "MapPlacePhone"
  case map_PlaceLat = "MapPlaceLat"
  case map_PlaceLon = "MapPlaceLon"
}
