//
//  LoyaltyCard.swift
//  appsplash
//
//  Created by Doug Chisholm on 10/03/2016.
//  Copyright © 2016 Doug Chisholm. All rights reserved.
//

import Foundation

struct LoyaltyCard {
	
	//MARK: - Private
	private init() {}
	
	private var id: Int = 0
	private var onetimeCode: String = ""
	private var masterCode: String = ""
	private var code: String = ""
	private var redemption: String = ""
	private var stampTheme: String = ""
	private var cooldown: Int = 0
	private var store = CardStore("loyaltyCard")
	private var data: CardStore.CardData!
	
	private mutating func refresh() {
		
		if let cardSettings =  store.getValue(for: "\(id)"){
			data = cardSettings
		} else{
			data = CardStore.CardData.init(id: id, numberOfCheckIns: 0, lastCheckIn: Date.distantPast, oneOffUsed: false)
			store.set(value: data, for: "\(id)")
		}
	}
	
	private var lastCheckin: Date {
		get {
			return data.lastCheckIn
		}
	}
	
	//MARK: - Public
	
	var stamps: Int = 0
	
	var checkins: Int {
		mutating get {
			self.refresh()
			return data.numberOfCheckIns
		}
	}
	
	init(data: FreshStamp) {
		
		self.stamps = data.card.stamps
		self.code = data.card.code
		
		if let onetimeCode = data.card.onetimeCode {
			self.onetimeCode = onetimeCode
		} else {
			self.onetimeCode = ""
		}
		
		self.masterCode = data.card.masterCode
		self.redemption = data.card.redemptionCode
		self.cooldown = data.card.cooldown
		self.id = data.id
		self.stampTheme = data.card.stampTheme
		self.refresh()
	}
	
	var canCheckIn: Bool {
		return lastCheckin.addHours(cooldown).isLessThanDate(Date())
	}
	
	var canRedeem: Bool {
		mutating get {
			return checkins == stamps
		}
	}
	
	mutating func checkin(code: String) -> Bool {
		
		if !canCheckIn {
			return false
		}
		
		if let oneOff = self.data.oneOffUsed {
			self.data.oneOffUsed = oneOff
		} else {
			self.data.oneOffUsed = false
		}
		
		if self.code != code && self.masterCode != code && self.onetimeCode != code {
			return false
		}
		
		if self.data.oneOffUsed && self.onetimeCode == code {
			return false
		}
		
		if self.code == code {
			self.data.lastCheckIn = Date()
		}
		
		if self.onetimeCode == code {
			self.data.lastCheckIn = Date()
			self.data.numberOfCheckIns += 2
			self.data.oneOffUsed = true
			
		} else {
			self.data.numberOfCheckIns += 1
		}
		store.set(value: data, for: "\(data.id)")
		
		GeowaveService.TrackLoyaltyFeedback(id: data.id, isScan: true, isRedeem: false, stamps: checkins)
		
		return true
	}
	
	mutating func redeem(){
		data.numberOfCheckIns = 0
		store.set(value: data, for: "\(data.id)")
		GeowaveService.TrackLoyaltyFeedback(id: data.id, isScan: false, isRedeem: true, stamps: checkins)
	}
}


