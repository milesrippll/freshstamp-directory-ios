//
//  CardManager.swift
//  freshstamp
//
//  Created by Doug Chisholm on 04/10/2017.
//  Copyright © 2017 rippll. All rights reserved.
//

import Foundation

class CardManager {

  private let myCardsKey = "UserCards"

  private init() {}

  private var store = UserCardStore("UserCardStoreKey")

  private var cards = [FreshStamp]()

  static let instance: CardManager = {

    let instance = CardManager()

    if let cards = instance.store.getValue(for: instance.myCardsKey) {
      instance.cards = cards
    }
    return instance
  }()
	
	// finds card by code in current user cards
	func findCardInMyCards(code: String) -> FreshStamp? {
    if let myCard = self.cards.first(where: { $0.card.code == code || $0.card.masterCode == code || $0.card.onetimeCode == code }) {

      return myCard
    }
    return nil
  }
	
	// finds card by code in current card store
  func findCardInStore(code: String) -> FreshStamp? {

    if let storeCard = FreshStampStore.instance.cards().first(where: { $0.card.code == code || $0.card.masterCode == code || $0.card.onetimeCode == code }) {
      self.cards.append(storeCard)
      self.store.set(value: self.cards, for: myCardsKey)
      return storeCard
    }

    return nil
  }
	
	// refreshes user's card data from API
  func refreshMyCardsFromAPI() {

    for (index, card) in self.cards.enumerated() {
      if let fsCard = FreshStampStore.instance.cards().first(where: { $0.id == card.id }) {
        self.cards[index] = fsCard
      }
    }
    self.store.set(value: self.cards, for: self.myCardsKey)
  }
	
	// returns all freshstamp cards
	func allCards() -> [FreshStamp] {
		return FreshStampStore.instance.cards()
	}
	
	// returns all user cards
  func myCards() -> [FreshStamp] {

    var myCards = self.cards
		
		// add fake card at the top for scan shortcut
    //myCards.insert(fakeCard, at: 0)
		
    return myCards
  }
	
	// fake card settings
//  var fakeCard: FreshStamp = {
//
//    let card = Card(
//			offer: "",
//			code: "",
//			masterCode: "",
//			onetimeCode: "",
//			redemptionCode: "",
//			stamps: 0,
//			cooldown: 0,
//			stampTheme: "",
//			backgroundColor: "000000",
//			textColor: "000000",
//			butonTextColor: "000000",
//			butonColor: "000000")
//
//    let business = Business(name: "", logoUrl: "wallet-add-card.png", webUrl: "", backgroundColor: "383838", yelpUrl: "")
//    let fsCard = FreshStamp(id: 0, card: card, business: business)
//
//	return fsCard
//  }()
}
